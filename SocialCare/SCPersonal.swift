//
//  SCPersonal.swift
//  SocialCare
//
//  Created by Mrinal Khullar on 4/29/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit
import QuartzCore

class SCPersonal: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0

    @IBOutlet weak var postCoverView: UIView!
    @IBOutlet weak var postUpdatesOutlet: UIButton!
    @IBOutlet weak var whatsNewTxtField: UITextField!
    @IBOutlet weak var personalTableView: UITableView!
    
    var myPersonalArray:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        fillMyPersonalLisitng()
        
        personalTableView.backgroundColor = UIColor.clearColor()
        
        postCoverView.layer.cornerRadius = 8
        postCoverView.layer.borderWidth = 4
        postCoverView.layer.borderColor = UIColor.clearColor().CGColor
        
        postUpdatesOutlet.layer.cornerRadius = 8
        postUpdatesOutlet.layer.borderWidth = 4
        postUpdatesOutlet.layer.borderColor = UIColor.clearColor().CGColor
        
        whatsNewTxtField.layer.cornerRadius = 8
        whatsNewTxtField.layer.borderWidth = 4
        whatsNewTxtField.layer.borderColor = UIColor.clearColor().CGColor
        
        let paddingViewUserName = UIView(frame: CGRectMake(0, 0, 15, whatsNewTxtField.frame.height))
        whatsNewTxtField.leftView = paddingViewUserName
        whatsNewTxtField.leftViewMode = UITextFieldViewMode.Always
        
        
        whatsNewTxtField.attributedPlaceholder = NSAttributedString(string:"WHAT'S NEW,ST?", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool)
    {
         closeMenuBtn.hidden = true
        
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
    }

    func fillMyPersonalLisitng()
    {
        myPersonalArray .addObject(["statusPersonal":"ST 10 CHANGED THEIR PROFILE PICTURE","personalImage":"image3.jpg","personalTime":"2 days,18 hours ago","comment":"0","Favorite":"0"])
       myPersonalArray .addObject(["statusPersonal":"ST 10 CHANGED THEIR PROFILE PICTURE","personalImage":"image4.jpeg","personalTime":"2 days,18 hours ago","comment":"0","Favorite":"0"])
       myPersonalArray .addObject(["statusPersonal":"ST 10 CHANGED THEIR PROFILE PICTURE","personalImage":"image3.jpg","personalTime":"2 days,18 hours ago","comment":"0","Favorite":"0"])
       myPersonalArray .addObject(["statusPersonal":"ST 10 CHANGED THEIR PROFILE PICTURE","personalImage":"image4.jpeg","personalTime":"2 days,18 hours ago","comment":"0","Favorite":"0"])
       myPersonalArray .addObject(["statusPersonal":"ST 10 CHANGED THEIR PROFILE PICTURE","personalImage":"image3.jpg","personalTime":"2 days,18 hours ago","comment":"0","Favorite":"0"])

        // print("locationListingArray is\(locationListingArray)")
    }
    
    @IBAction func menuBtn(sender: AnyObject)
    {
       myMenuView.hidden = false
        
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
        else
        {
            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.5, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func favoriteBtn(sender: AnyObject) {
    }
    
    @IBAction func mentionBtn(sender: AnyObject) {
    }

    @IBAction func postUpdatesBtn(sender: AnyObject) {
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 115.0
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1;
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return myPersonalArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("SCCustomCell", forIndexPath: indexPath) as! SCCustomCell
        
        
        cell.personalBlueView.layer.cornerRadius = 8
        cell.personalBlueView.layer.borderWidth = 3
        cell.personalBlueView.layer.borderColor = UIColor(red: 70.0/255.0, green: 139.0/255.0, blue: 185.0/255.0, alpha: 1.0).CGColor
        
        cell.personalImageView.layer.borderWidth = 2
        cell.personalImageView.layer.cornerRadius =  cell.personalImageView.frame.size.width/2
        cell.personalImageView.layer.borderColor = UIColor.init(white: 0.5, alpha: 1.0).CGColor
        cell.personalImageView.clipsToBounds = true
        cell.personalImageView!.contentMode = UIViewContentMode.ScaleAspectFill
      
        cell.statusDescLabel.text = myPersonalArray[indexPath.row].valueForKey("statusPersonal") as? String
        let courseImages = myPersonalArray[indexPath.row].valueForKey("personalImage") as? String
        cell.personalImageView.image = UIImage(named:courseImages!)
        cell.favorite.text = myPersonalArray[indexPath.row].valueForKey("Favorite") as? String
        cell.comment.text = myPersonalArray[indexPath.row].valueForKey("comment") as? String
        cell.timepersonal.text = myPersonalArray[indexPath.row].valueForKey("personalTime") as? String
        
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
        // Cell.separatorInset = UIEdgeInsetsZero;
        tableView.separatorInset = UIEdgeInsetsZero;
        tableView.separatorColor = UIColor.clearColor()
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        
    }
}
