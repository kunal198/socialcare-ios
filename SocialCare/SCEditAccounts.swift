//
//  SCEditAccounts.swift
//  SocialCare
//
//  Created by Mrinal Khullar on 4/29/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class SCEditAccounts: UIViewController
{
    
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0

    @IBOutlet weak var redPutlet: UIButton!
    @IBOutlet weak var redView: UIView!
    @IBOutlet weak var confirmPasswordEdit: UITextField!
    @IBOutlet weak var newPasswordEdit: UITextField!
    @IBOutlet weak var emailEdit: UITextField!
    @IBOutlet weak var displayCertificateEdit: UITextField!
    @IBOutlet weak var lastNameEdit: UITextField!
    @IBOutlet weak var firstNameEdit: UITextField!
    @IBOutlet weak var usernameEdit: UITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        placeHolderMethodForEditing()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool)
    {
         closeMenuBtn.hidden = true
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func menuBtn(sender: AnyObject)
    {
        myMenuView.hidden = false
        
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
        else
        {
            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.5, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })
    }
    @IBAction func saveBtn(sender: AnyObject) {
    }
    
    func placeHolderMethodForEditing()
    {
        redPutlet.layer.cornerRadius = 8
        redPutlet.layer.borderWidth = 4
        redPutlet.layer.borderColor = UIColor.clearColor().CGColor
        
        redView.layer.cornerRadius = 8
        redView.layer.borderWidth = 4
        redView.layer.borderColor = UIColor.clearColor().CGColor
        
        confirmPasswordEdit.layer.cornerRadius = 8
        confirmPasswordEdit.layer.borderWidth = 4
        confirmPasswordEdit.layer.borderColor = UIColor.clearColor().CGColor
        
        newPasswordEdit.layer.cornerRadius = 8
        newPasswordEdit.layer.borderWidth = 4
        newPasswordEdit.layer.borderColor = UIColor.clearColor().CGColor
        
        emailEdit.layer.cornerRadius = 8
        emailEdit.layer.borderWidth = 4
        emailEdit.layer.borderColor = UIColor.clearColor().CGColor
        
        lastNameEdit.layer.cornerRadius = 8
        lastNameEdit.layer.borderWidth = 4
        lastNameEdit.layer.borderColor = UIColor.clearColor().CGColor
        
        displayCertificateEdit.layer.cornerRadius = 8
        displayCertificateEdit.layer.borderWidth = 4
        displayCertificateEdit.layer.borderColor = UIColor.clearColor().CGColor
        
        firstNameEdit.layer.cornerRadius = 8
        firstNameEdit.layer.borderWidth = 4
        firstNameEdit.layer.borderColor = UIColor.clearColor().CGColor
        
        usernameEdit.layer.cornerRadius = 8
        usernameEdit.layer.borderWidth = 4
        usernameEdit.layer.borderColor = UIColor.clearColor().CGColor
        
        let paddingView = UIView(frame: CGRectMake(0, 0, 15, confirmPasswordEdit.frame.height))
        confirmPasswordEdit.leftView = paddingView
        confirmPasswordEdit.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewPwd = UIView(frame: CGRectMake(0, 0, 15, newPasswordEdit.frame.height))
        newPasswordEdit.leftView = paddingViewPwd
        newPasswordEdit.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewChildren = UIView(frame: CGRectMake(0, 0, 15, emailEdit.frame.height))
        emailEdit.leftView = paddingViewChildren
        emailEdit.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewlastName = UIView(frame: CGRectMake(0, 0, 15, lastNameEdit.frame.height))
        lastNameEdit.leftView = paddingViewlastName
        lastNameEdit.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewDisplay = UIView(frame: CGRectMake(0, 0, 15, displayCertificateEdit.frame.height))
        displayCertificateEdit.leftView = paddingViewDisplay
        displayCertificateEdit.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewFirstName = UIView(frame: CGRectMake(0, 0, 15, firstNameEdit.frame.height))
        firstNameEdit.leftView = paddingViewFirstName
        firstNameEdit.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewUserName = UIView(frame: CGRectMake(0, 0, 15, usernameEdit.frame.height))
        usernameEdit.leftView = paddingViewUserName
        usernameEdit.leftViewMode = UITextFieldViewMode.Always
        
        
        confirmPasswordEdit.attributedPlaceholder = NSAttributedString(string:"CONFIRM PASSWORD", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
        newPasswordEdit.attributedPlaceholder = NSAttributedString(string:"NEW PASSWORD", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
        
        emailEdit.attributedPlaceholder = NSAttributedString(string:"EMAIL (REQUIRED)", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
        
        lastNameEdit.attributedPlaceholder = NSAttributedString(string:"ENTER MESSAGE", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
        displayCertificateEdit.attributedPlaceholder = NSAttributedString(string:"DISPLAY TO CERTIFICATE & FULL NAME AS", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
        
        firstNameEdit.attributedPlaceholder = NSAttributedString(string:"FIRST NAME", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
        usernameEdit.attributedPlaceholder = NSAttributedString(string:"USERNAME", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
    }
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
}
