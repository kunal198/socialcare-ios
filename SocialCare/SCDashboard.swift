//
//  SCDashboard.swift
//  SocialCare
//
//  Created by Mrinal Khullar on 4/25/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit
import MobileCoreServices

class SCDashboard: UIViewController,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0
    

    @IBOutlet weak var blackViewOutlet: UIButton!
    @IBOutlet weak var blackView: UIView!
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var uploaImgaeOutlet: UIButton!
    @IBOutlet weak var uploadImageView: UIImageView!
    @IBOutlet weak var darkBlueView: UIView!
    @IBOutlet weak var purpleView: UIView!
    @IBOutlet weak var greenView: UIView!
    @IBOutlet weak var skyBlueView: UIView!
    @IBOutlet weak var orangeView: UIView!
    @IBOutlet weak var redView: UIView!
    @IBOutlet weak var clearView: UIView!
    
    /////////////////////////////////////////
    
    var isMenuViewVisible = Bool()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        borderCornerRadius()
        menuView.hidden = true
        blackView.hidden = true
        blackViewOutlet.hidden = true

        // Do any additional setup after loading the view.
    }
    
    func borderCornerRadius()
    {
        darkBlueView.layer.cornerRadius = 8
        darkBlueView.layer.borderWidth = 2
        darkBlueView.layer.borderColor = UIColor(red: 55/255, green: 115/255, blue:188/255, alpha: 1).CGColor
        
        uploadImageView.layer.cornerRadius = uploadImageView.frame.size.height/2
        uploadImageView.layer.borderWidth = 2
        uploadImageView.layer.borderColor = UIColor.clearColor().CGColor
        uploadImageView.clipsToBounds = true
        
        purpleView.layer.cornerRadius = 8
        purpleView.layer.borderWidth = 2
        purpleView.layer.borderColor =  UIColor(red: 217/255, green: 60/255, blue:212/255, alpha: 1).CGColor
        
        greenView.layer.cornerRadius = 8
        greenView.layer.borderWidth = 2
        greenView.layer.borderColor = UIColor(red: 72/255, green: 222/255, blue:135/255, alpha: 1).CGColor
        
        skyBlueView.layer.cornerRadius = 8
        skyBlueView.layer.borderWidth = 2
        skyBlueView.layer.borderColor =  UIColor(red: 86/255, green: 193/255, blue:255/255, alpha: 1).CGColor
        
        orangeView.layer.cornerRadius = 8
        orangeView.layer.borderWidth = 2
        orangeView.layer.borderColor =  UIColor(red: 239/255, green: 152/255, blue:67/255, alpha: 1).CGColor
        
        redView.layer.cornerRadius = 8
        redView.layer.borderWidth = 2
        redView.layer.borderColor =  UIColor(red: 237/255, green: 96/255, blue:101/255, alpha: 1).CGColor
        
        clearView.layer.cornerRadius = 8
        clearView.layer.borderWidth = 2
        clearView.layer.borderColor = UIColor.clearColor().CGColor
        
//        uploaImgaeOutlet.layer.cornerRadius = 6
        uploaImgaeOutlet.layer.borderWidth = 2
        uploaImgaeOutlet.layer.borderColor = UIColor.whiteColor().CGColor
    }

    override func viewWillAppear(animated: Bool)
    {
         closeMenuBtn.hidden = true
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func uploadImageBtn(sender: AnyObject)
    {
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Camera", "Gallery")
        actionSheet.tag = sender.tag
        actionSheet.showInView(self.view)
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        switch (buttonIndex)
        {
        case 0:
            print("Cancel")
        case 1:
            Camera()
        case 2:
            Gallery()
        default:
            print("Default")
            
        }
    }
    
    func Camera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
        {
            // print("Camera capture")
            
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.Camera;
            imag.mediaTypes = [kUTTypeImage as String]
            imag.allowsEditing = false
            
            self.presentViewController(imag, animated: true, completion: nil)
        }
    }
    
    func Gallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
            imag.mediaTypes = [kUTTypeImage as String]
            imag.allowsEditing = false
            self.presentViewController(imag, animated: true, completion: nil)
        }
        else
        {
            NSLog("failed")
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?)
    {
        // print("ISShowBarcodeDetail.swift delegate method")
        self.dismissViewControllerAnimated(false, completion: nil)
        
        uploadImageView.image = image
    }

    @IBAction func blackViewBtn(sender: AnyObject)
    {
        menuView.hidden = true
        blackView.hidden = true
        blackViewOutlet.hidden = true
    }
    @IBAction func logoutBtn(sender: AnyObject)
    {
        let SCLoginz = self.storyboard!.instantiateViewControllerWithIdentifier("SCLogin") as! SCLogin
        
        self.navigationController?.pushViewController(SCLoginz, animated: true)
    }
    @IBAction func messagesBtn(sender: AnyObject)
    {
        let SCSentz = self.storyboard!.instantiateViewControllerWithIdentifier("SCSent") as! SCSent
        
        self.navigationController?.pushViewController(SCSentz, animated: true)
    }
    @IBAction func profileBtn(sender: AnyObject)
    {
        let SCPersonalZ = self.storyboard!.instantiateViewControllerWithIdentifier("SCPersonal") as! SCPersonal
        
        self.navigationController?.pushViewController(SCPersonalZ, animated: true)
    }
    @IBAction func acitivityBtn(sender: AnyObject) {
    }
    @IBAction func editAccountsBtn(sender: AnyObject)
    {
        let SCEditAccountsz = self.storyboard!.instantiateViewControllerWithIdentifier("SCEditAccounts") as! SCEditAccounts
        
        self.navigationController?.pushViewController(SCEditAccountsz, animated: true)
    }
    @IBAction func accessCertificatesBtn(sender: AnyObject) {
    }
    @IBAction func requestACourseBtn(sender: AnyObject) {
    }
    @IBAction func myCoursesBtn(sender: AnyObject)
    {
        let SCMyCoursesz = self.storyboard!.instantiateViewControllerWithIdentifier("SCMyCourses") as! SCMyCourses
        
        self.navigationController?.pushViewController(SCMyCoursesz, animated: true)
    }
    @IBAction func dashboardBtn(sender: AnyObject)
    {
        
    }

    @IBAction func menuBtn(sender: AnyObject)
    {
        myMenuView.hidden = false
        
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
        else
        {
            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.5, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })
//        menuView.hidden = false
//        blackView.hidden = false
//        blackViewOutlet.hidden = false
        
    }
    @IBAction func backBtn(sender: AnyObject)
    {
        myMenuView.hidden = false
        
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
        else
        {
            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.5, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })
        
        self.navigationController?.popViewControllerAnimated(true)
    }
}
