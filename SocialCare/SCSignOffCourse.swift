
//
//  SCSignOffCourse.swift
//  SocialCare
//
//  Created by Mrinal Khullar on 4/28/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class SCSignOffCourse: UIViewController
{
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0

    @IBOutlet weak var resetOutlet: UIButton!
    @IBOutlet weak var resetCoverView: UIView!
    @IBOutlet weak var dashBookOutlet: UIButton!
    @IBOutlet weak var dashboardCoverView: UIView!
    @IBOutlet weak var handbookOutlet: UIButton!
    @IBOutlet weak var handbookCoverView: UIView!
    @IBOutlet weak var upperRedView: UIView!
    @IBOutlet weak var upperCoverView: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        BorderColrMethods()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool)
    {
         closeMenuBtn.hidden = true
        
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menuBtn(sender: AnyObject)
    {
        myMenuView.hidden = false
        
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
        else
        {
            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.5, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })
    }
    func BorderColrMethods()
    {
        resetOutlet.layer.cornerRadius = 8
        resetOutlet.layer.borderWidth = 4
        resetOutlet.layer.borderColor = UIColor.clearColor().CGColor
        
        resetCoverView.layer.cornerRadius = 8
        resetCoverView.layer.borderWidth = 4
        resetCoverView.layer.borderColor = UIColor.clearColor().CGColor
        
        dashBookOutlet.layer.cornerRadius = 8
        dashBookOutlet.layer.borderWidth = 4
        dashBookOutlet.layer.borderColor = UIColor.clearColor().CGColor
        
        dashboardCoverView.layer.cornerRadius = 8
        dashboardCoverView.layer.borderWidth = 4
        dashboardCoverView.layer.borderColor = UIColor.clearColor().CGColor
        
        handbookOutlet.layer.cornerRadius = 8
        handbookOutlet.layer.borderWidth = 4
        handbookOutlet.layer.borderColor = UIColor.clearColor().CGColor
        
        handbookCoverView.layer.cornerRadius = 8
        handbookCoverView.layer.borderWidth = 4
        handbookCoverView.layer.borderColor = UIColor.clearColor().CGColor
        
        upperRedView.layer.cornerRadius = 8
        upperRedView.layer.borderWidth = 4
        upperRedView.layer.borderColor = UIColor.clearColor().CGColor
        
        upperCoverView.layer.cornerRadius = 8
        upperCoverView.layer.borderWidth = 4
        upperCoverView.layer.borderColor = UIColor.clearColor().CGColor
        

    }
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func handbookBtn(sender: AnyObject) {
    }


    @IBAction func dashboardBtn(sender: AnyObject) {
    }

    @IBAction func resetBtn(sender: AnyObject) {
    }
}
