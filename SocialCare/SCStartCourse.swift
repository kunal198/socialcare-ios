//
//  SCStartCourse.swift
//  SocialCare
//
//  Created by Mrinal Khullar on 4/26/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit
import QuartzCore

var isUnitStarted:Bool = Bool()
var isUnitStartedVisible:Bool = Bool()
var isUnitSecond:Bool = Bool()
var isUnitOne:Bool = Bool()
var isUnitFour:Bool = Bool()

class SCStartCourse: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0
    
    @IBOutlet weak var unitTableView: UITableView!

    
    //////////////////////////
    
    var unitArray:NSMutableArray = NSMutableArray()
    var expandedUnitArray:NSMutableArray = NSMutableArray()
    var UnitOneIndex = 0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        unitTableView.backgroundColor = UIColor.clearColor()
        fillUnitArray()
        fillExpandedUnitArray()
        
        unitTableView.sectionHeaderHeight = 0
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool)
    {
         closeMenuBtn.hidden = true
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func fillUnitArray()
    {
        unitArray .addObject(["unitNumber":" ","unitAbout": "WELCOME TO THE START OF YOUR COURSE.PLEASE MAKE SURE YOU HAVE PLENTY OF PENS AND PAPER TO MAKE NOTES.WHEN READY CLICK START AND GOOD LUCK.","unitYellow":"IMPORTANT - PLEASE MAKE SURE THAT YOUR VOLUME IS FULLY TURNED UP BEFORE YOU START THE COURSE."])
        unitArray .addObject(["unitNumber":"1","unitAbout":"Unit 1 is the online aprt of your course.You can redo unit 1 as many times as you like before sign off.","unitYellow":""])
        unitArray .addObject(["unitNumber":"2","unitAbout":"Unit 2 is a set of 10 random questions bases on unit 1.Once you have completed this assessment you will see your answers and be able to go back to redo any you have got wrong.","unitYellow":""])
          unitArray .addObject(["unitNumber":"3","unitAbout":"Unit 3 is a extension handbook related to the course.You can alse download this anytime with your account.","unitYellow":""])
          unitArray .addObject(["unitNumber":"4","unitAbout":"Unit 4 is your Learning Outcome Review.This will be used to demonstrate to Ofsted what you have learned from the course.this will be reviewed and accessed by your direct supervisor.","unitYellow":""])
        unitArray .addObject(["unitNumber":" ","unitAbout": "IMPORTANT BEFORE YOU SIGN OFF YOUR COURSE PLEASE REVIEW YOUR QUESTIONS AND ANSWERS ABOVE.\nYOU CAN IMPROVE YOUR SCORE BY RE-DOING ANY QUESTIONS YOU HAVE GOT WRONG.\nIF FEEL YOU TO REDO OF THE UNITS YOU MUST DO THIS BEFORE YOU SIGN OFF YOUR COURSE.\nIF YOU ARE HAPPY WITH ALL OF THE ABOVE PLEASE CLICK ON SIGN OFF MY COURSES.","unitYellow":""])
        
        // print("locationListingArray is\(locationListingArray)")
    }
    func fillExpandedUnitArray()
    {
        expandedUnitArray .addObject(["unitNumber":" ","unitAbout": "WELCOME TO THE START OF YOUR COURSE.PLEASE MAKE SURE YOU HAVE PLENTY OF PENS AND PAPER TO MAKE NOTES.WHEN READY CLICK START AND GOOD LUCK.","unitYellow":"IMPORTANT - PLEASE MAKE SURE THAT YOUR VOLUME IS FULLY TURNED UP BEFORE YOU START THE COURSE.","quesNo":"","quesname":"","answer":""])
        expandedUnitArray .addObject(["unitNumber":"1","unitAbout":"Unit 1 is the online aprt of your course.You can redo unit 1 as many times as you like before sign off.","unitYellow":"","quesNo":"","quesname":"","answer":""])
        expandedUnitArray .addObject(["unitNumber":"2","unitAbout":"Unit 2 is a set of 10 random questions bases on unit 1.Once you have completed this assessment you will see your answers and be able to go back to redo any you have got wrong.","unitYellow":"","quesNo":"","quesname":"","answer":""])
        expandedUnitArray .addObject(["unitNumber":" ","unitAbout":"PLEASE REVIEW YOUR ANSWERS BELOW AND REDO ANY YOU FEEL YOU IMPROVE UPON?IF HAPPY WITH ALL OF YOUR ANSWERS PLEASE SCROLL DOWN AND CLICK ON UNIT 3.","unitYellow":"YOUR CURRENT SCORE IS 0/100 WHICH IS A FAIL.WE STRONGLY SUGGEST YOU REDO ANY QUESTIONS YOU HAVE ANSWERED IN-CORRECTLY TO IMPROVE YOURSCORE.","quesNo":"","quesname":"","answer":""])
        
        expandedUnitArray .addObject(["unitNumber":" ","unitAbout":"","unitYellow":"","quesNo":"Q1","quesname":"What is an STI?","answer":" Sexually Turbo Injection"])
        
        expandedUnitArray .addObject(["unitNumber":" ","unitAbout":"","unitYellow":"","quesNo":"Q2","quesname":"What is symptom of chlampdia?","answer":" Safer transmitted Infection"])
        
        expandedUnitArray .addObject(["unitNumber":"3","unitAbout":"Unit 3 is a extension handbook related to the course.You can alse download this anytime with your account.","unitYellow":""])
        
        expandedUnitArray .addObject(["unitNumber":"4","unitAbout":"Unit 4 is your Learning Outcome Review.This will be used to demonstrate to Ofsted what you have learned from the course.this will be reviewed and accessed by your direct supervisor.","unitYellow":""])
        expandedUnitArray .addObject(["unitNumber":" ","unitAbout": "IMPORTANT BEFORE YOU SIGN OFF YOUR COURSE PLEASE REVIEW YOUR QUESTIONS AND ANSWERS ABOVE.\nYOU CAN IMPROVE YOUR SCORE BY RE-DOING ANY QUESTIONS YOU HAVE GOT WRONG.\nIF FEEL YOU TO REDO OF THE UNITS YOU MUST DO THIS BEFORE YOU SIGN OFF YOUR COURSE.\nIF YOU ARE HAPPY WITH ALL OF THE ABOVE PLEASE CLICK ON SIGN OFF MY COURSES.","unitYellow":""])
        
        // print("locationListingArray is\(locationListingArray)")
    }
    @IBAction func menuBtn(sender: AnyObject)
    {
        myMenuView.hidden = false
        
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
        else
        {
            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.5, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 0
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if(isUnitStartedVisible == true)
        {
            if(indexPath.row == 0)
            {
                return 200.0
            }
            if(indexPath.row == 3)
            {
                return 200.0
            }
            if(indexPath.row == 8)
            {
                return 200.0
            }
            else
            {
                return 150.0
            }
            
        }
        else
        {
            if(indexPath.row == 0)
            {
                return 200.0
            }
            if(indexPath.row == 5)
            {
                return 250.0
            }
            else
            {
                return 150.0
            }
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1;
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
         if(isUnitStartedVisible == true)
         {
            return expandedUnitArray.count
         }
         else
         {
            return unitArray.count
         }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("SCCustomCell", forIndexPath: indexPath) as! SCCustomCell
        
        cell.signOffCover.layer.cornerRadius = 8
        cell.signOffCover.layer.borderWidth = 2
        cell.signOffCover.layer.borderColor = UIColor.clearColor().CGColor
        
        cell.startUnitCoverView.layer.cornerRadius = 8
        cell.startUnitCoverView.layer.borderWidth = 2
        cell.startUnitCoverView.layer.borderColor = UIColor.clearColor().CGColor
        
        cell.whiteRedView.layer.cornerRadius = 8
        cell.whiteRedView.layer.borderWidth = 2
        cell.whiteRedView.layer.borderColor = UIColor.clearColor().CGColor
        //        cell.whiteRedView.clipsToBounds = true
        
        cell.unitNumber.layer.cornerRadius = cell.unitNumber.frame.size.width/2
        cell.unitNumber.layer.borderWidth = 2
        cell.unitNumber.layer.borderColor = UIColor.init(white: 1.0, alpha: 0.5).CGColor
        cell.unitNumber.clipsToBounds = true
        
        cell.unitView.layer.cornerRadius = 8
        cell.unitView.layer.borderWidth = 2
        cell.unitView.layer.borderColor = UIColor.clearColor().CGColor
        cell.unitView.clipsToBounds = true
        
        cell.startUnitLbl.layer.cornerRadius = 8
        cell.startUnitLbl.layer.borderWidth = 4
        cell.startUnitLbl.layer.borderColor = UIColor.clearColor().CGColor
        
        cell.startOff.layer.cornerRadius = 8
        cell.startOff.layer.borderWidth = 4
        cell.startOff.layer.borderColor = UIColor.clearColor().CGColor
        
        cell.unitAbout.textColor = UIColor.whiteColor()
        cell.unitAbout.numberOfLines = 4
        
        
        cell.answer.layer.cornerRadius = 8
        cell.answer.layer.borderWidth = 4
        cell.answer.layer.borderColor = UIColor.clearColor().CGColor
        cell.answer.clipsToBounds = true
        
        cell.correctInCorrectLbl.layer.cornerRadius = 8
        cell.correctInCorrectLbl.layer.borderWidth = 4
        cell.correctInCorrectLbl.layer.borderColor = UIColor.clearColor().CGColor
        
        cell.redoOultet.layer.cornerRadius = 8
        cell.redoOultet.layer.borderWidth = 4
        cell.redoOultet.layer.borderColor = UIColor.clearColor().CGColor
        cell.redoOultet.backgroundColor = UIColor(red: 228.0/255.0, green: 185.0/255.0, blue: 43.0/255.0, alpha: 1.0)

        
        if (isUnitStartedVisible == true)
        {
            cell.unitAbout.text = expandedUnitArray[indexPath.row].valueForKey("unitAbout") as? String
            cell.unitNumber.text = expandedUnitArray[indexPath.row].valueForKey("unitNumber") as? String
            
    
            print("isUnitStartedVisible == true")
            if(indexPath.row == 0)
            {
                cell.signOffCover.hidden = true
                cell.unitAbout.font = UIFont(name:"HelveticaNeue-Bold", size: 11.0)
                cell.yellowUnitAbout.font = UIFont(name:"HelveticaNeue-Bold", size: 11.0)
     
                cell.startUnitCoverView.hidden = true
                cell.whiteRedView.hidden = true
                cell.questionNo.hidden = true
                cell.quesName.hidden = true
                cell.answer.hidden = true
                cell.correctInCorrectLbl.hidden = true
                cell.redoOultet.hidden = true
                
                cell.unitView.layer.borderColor = UIColor.clearColor().CGColor
                
                cell.unitAbout.text =  expandedUnitArray[indexPath.row].valueForKey("unitAbout") as? String
                cell.yellowUnitAbout.text = expandedUnitArray[indexPath.row].valueForKey("unitYellow") as? String
//                cell.yellowUnitAbout.font =  cell.yellowUnitAbout.font.fontWithSize(10)
//                cell.unitAbout.font =  cell.unitAbout.font.fontWithSize(10)
                cell.yellowUnitAbout.numberOfLines = 15
                cell.unitView.backgroundColor = UIColor.clearColor()
                cell.unitNumber.hidden = true
                cell.startUnitLbl.hidden = true
                cell.startOff.hidden = true
                cell.yellowUnitAbout.hidden = false
            }
            if(indexPath.row == 1 || indexPath.row == 2)
            {
                cell.signOffCover.hidden = true
                cell.questionNo.hidden = true
                cell.quesName.hidden = true
                cell.answer.hidden = true
                cell.correctInCorrectLbl.hidden = true
                
                cell.unitNumber.hidden = false
                cell.startUnitLbl.hidden = false
                cell.unitView.hidden = false
                cell.startOff.hidden = true
                cell.yellowUnitAbout.hidden = true
                cell.unitView.backgroundColor = UIColor(red: 252.0/255.0, green: 61.0/255.0, blue: 61.0/255.0, alpha: 1.0)
                cell.startUnitLbl.backgroundColor = UIColor(red: 26.0/255.0, green: 94.0/255.0, blue: 139.0/255.0, alpha: 1.0)
                cell.unitNumber.backgroundColor = UIColor(red: 26.0/255.0, green: 94.0/255.0, blue: 139.0/255.0, alpha: 1.0)
                if(indexPath.row == 1)
                {
                    cell.startUnitLbl.setTitle("RE-START UNIT", forState: .Normal)
                }
                if (indexPath.row == 2)
                {
                    cell.startUnitCoverView.hidden = true
                    cell.startUnitLbl.hidden = true
                }
                
            }
            if(indexPath.row == 3)
            {
                cell.signOffCover.hidden = true
                cell.unitAbout.font = UIFont(name:"HelveticaNeue-Bold", size: 11.0)
                cell.yellowUnitAbout.font = UIFont(name:"HelveticaNeue-Bold", size: 11.0)
                
                cell.startUnitCoverView.hidden = true
                cell.whiteRedView.hidden = true
                cell.questionNo.hidden = true
                cell.quesName.hidden = true
                cell.answer.hidden = true
                
                cell.unitView.layer.borderColor = UIColor.clearColor().CGColor
                
                cell.unitAbout.text =  expandedUnitArray[indexPath.row].valueForKey("unitAbout") as? String
                cell.yellowUnitAbout.text = expandedUnitArray[indexPath.row].valueForKey("unitYellow") as? String
//                cell.yellowUnitAbout.font =  cell.yellowUnitAbout.font.fontWithSize(10)
//                cell.unitAbout.font =  cell.unitAbout.font.fontWithSize(10)
                cell.yellowUnitAbout.numberOfLines = 15
                cell.unitView.backgroundColor = UIColor.clearColor()
                cell.unitNumber.hidden = true
                cell.startUnitLbl.hidden = true
                cell.startOff.hidden = true
                cell.yellowUnitAbout.hidden = false
                
            }
            if(indexPath.row == 4 || indexPath.row == 5)
            {
                cell.whiteRedView.hidden = false
                cell.signOffCover.hidden = true
                cell.questionNo.hidden = false
                cell.quesName.hidden = false
                cell.answer.hidden = false
                cell.correctInCorrectLbl.hidden = false
                
                cell.unitView.backgroundColor = UIColor(red: 26.0/255.0, green: 94.0/255.0, blue: 139.0/255.0, alpha: 1.0)
                cell.questionNo.textColor = UIColor(red: 237.0/255.0, green: 221.0/255.0, blue: 106.0/255.0, alpha: 1.0)
                cell.answer.backgroundColor = UIColor(red: 70.0/255.0, green: 139.0/255.0, blue: 185.0/255.0, alpha: 1.0)

                
                cell.unitNumber.hidden = true
                cell.startUnitLbl.hidden = true
                cell.startOff.hidden = true
                cell.yellowUnitAbout.hidden = true
                cell.startUnitLbl.enabled = true
                cell.questionNo.text =  expandedUnitArray[indexPath.row].valueForKey("quesNo") as? String
                cell.quesName.text =  expandedUnitArray[indexPath.row].valueForKey("quesname") as? String
                cell.answer.text =  expandedUnitArray[indexPath.row].valueForKey("answer") as? String
                
                if(indexPath.row == 5)
                {
                    cell.redoOultet.hidden = true
                    cell.correctInCorrectLbl.setTitle("CORRECT", forState: .Normal)
                     cell.correctInCorrectLbl.backgroundColor = UIColor(red: 60.0/255.0, green: 152.0/255.0, blue: 206.0/255.0, alpha: 1.0)
                }
                else
                {
                    cell.startUnitCoverView.hidden = true
                    cell.redoOultet.hidden = false
                    cell.redoOultet.setTitle("REDO", forState: .Normal)
                    cell.correctInCorrectLbl.setTitle("INCORRECT", forState: .Normal)
                    cell.correctInCorrectLbl.backgroundColor = UIColor(red: 252.0/255.0, green: 61.0/255.0, blue: 61.0/255.0, alpha: 1.0)
                }

            }
            if(indexPath.row == 6)
            {
                cell.signOffCover.hidden = true
                cell.questionNo.hidden = true
                cell.quesName.hidden = true
                cell.answer.hidden = true
                
                cell.unitNumber.hidden = false
                cell.startUnitLbl.hidden = false
                cell.unitView.hidden = false
                cell.startOff.hidden = true
                cell.yellowUnitAbout.hidden = true
                cell.unitView.backgroundColor = UIColor(red: 252.0/255.0, green: 61.0/255.0, blue: 61.0/255.0, alpha: 1.0)
                cell.startUnitLbl.backgroundColor = UIColor(red: 26.0/255.0, green: 94.0/255.0, blue: 139.0/255.0, alpha: 1.0)
                cell.unitNumber.backgroundColor = UIColor(red: 26.0/255.0, green: 94.0/255.0, blue: 139.0/255.0, alpha: 1.0)
                
                if(isUnitFour == true)
                {
                    cell.startUnitLbl.setTitle(" RE-START UNIT ", forState: .Normal)
                }
                else
                {
                    cell.startUnitLbl.setTitle(" START UNIT ", forState: .Normal)
                }
            }
            if(indexPath.row == 7)
            {
                cell.signOffCover.hidden = true
                cell.questionNo.hidden = true
                cell.quesName.hidden = true
                cell.answer.hidden = true
                
                cell.unitView.backgroundColor = UIColor.lightGrayColor()
                cell.unitNumber.backgroundColor = UIColor.darkGrayColor()
                cell.startOff.hidden = true
                cell.yellowUnitAbout.hidden = true
                cell.startUnitLbl.hidden = false
                cell.startUnitLbl.enabled = false
                cell.startUnitLbl.backgroundColor = UIColor.darkGrayColor()
                cell.correctInCorrectLbl.hidden = true
                cell.redoOultet.hidden = true
                
                if(isUnitFour == true)
                {
                    
                    cell.startUnitLbl.setTitle("  START UNIT  ", forState: .Normal)
                    cell.startUnitLbl.enabled = true
                    cell.unitView.backgroundColor = UIColor(red: 252.0/255.0, green: 61.0/255.0, blue: 61.0/255.0, alpha: 1.0)
                    cell.startUnitLbl.backgroundColor = UIColor(red: 26.0/255.0, green: 94.0/255.0, blue: 139.0/255.0, alpha: 1.0)
                     cell.unitNumber.backgroundColor = UIColor(red: 26.0/255.0, green: 94.0/255.0, blue: 139.0/255.0, alpha: 1.0)
                }
            }
            if(indexPath.row == 8)
            {
                 cell.signOffCover.hidden = false
                cell.unitAbout.font = UIFont(name:"HelveticaNeue-Bold", size: 11.0)
                cell.yellowUnitAbout.font = UIFont(name:"HelveticaNeue-Bold", size: 11.0)
                
                cell.startUnitCoverView.hidden = true
                cell.whiteRedView.hidden = true
                cell.questionNo.hidden = true
                cell.quesName.hidden = true
                cell.answer.hidden = true
                cell.correctInCorrectLbl.hidden = true
                cell.redoOultet.hidden = true
                
                cell.unitView.layer.borderColor = UIColor.clearColor().CGColor
                
                cell.unitAbout.text =  expandedUnitArray[indexPath.row].valueForKey("unitAbout") as? String
//                cell.unitAbout.font =  cell.unitAbout.font.fontWithSize(9)
                cell.unitAbout.numberOfLines = 15
                cell.unitView.backgroundColor = UIColor.clearColor()
                cell.unitNumber.hidden = true
                cell.startUnitLbl.hidden = true
                cell.startOff.hidden = false
                cell.yellowUnitAbout.hidden = true
                cell.startUnitLbl.enabled = false
            }
        }
        else
        {
            cell.unitAbout.text = unitArray[indexPath.row].valueForKey("unitAbout") as? String
            cell.unitNumber.text = unitArray[indexPath.row].valueForKey("unitNumber") as? String

            if(indexPath.row == 0)
            {
                cell.signOffCover.hidden = true
                cell.unitAbout.font = UIFont(name:"HelveticaNeue-Bold", size: 11.0)
                cell.yellowUnitAbout.font = UIFont(name:"HelveticaNeue-Bold", size: 11.0)
                
                cell.startUnitCoverView.hidden = true
                cell.whiteRedView.hidden = true
                cell.unitView.layer.borderColor = UIColor.clearColor().CGColor
                
                cell.unitAbout.text =  unitArray[indexPath.row].valueForKey("unitAbout") as? String
                cell.yellowUnitAbout.text = unitArray[indexPath.row].valueForKey("unitYellow") as? String
//                cell.yellowUnitAbout.font =  cell.yellowUnitAbout.font.fontWithSize(10)
//                cell.unitAbout.font =  cell.unitAbout.font.fontWithSize(10)
                cell.yellowUnitAbout.numberOfLines = 15
                cell.unitView.backgroundColor = UIColor.clearColor()
                cell.unitNumber.hidden = true
                cell.startUnitLbl.hidden = true
                cell.startOff.hidden = true
                cell.yellowUnitAbout.hidden = false
            }
            if(indexPath.row == 1)
            {
                cell.signOffCover.hidden = true
                cell.unitNumber.hidden = false
                cell.startUnitLbl.hidden = false
                cell.unitView.hidden = false
                cell.startOff.hidden = true
                cell.yellowUnitAbout.hidden = true
                cell.unitView.backgroundColor = UIColor(red: 252.0/255.0, green: 61.0/255.0, blue: 61.0/255.0, alpha: 1.0)
                cell.startUnitLbl.backgroundColor = UIColor(red: 26.0/255.0, green: 94.0/255.0, blue: 139.0/255.0, alpha: 1.0)
                cell.unitNumber.backgroundColor = UIColor(red: 26.0/255.0, green: 94.0/255.0, blue: 139.0/255.0, alpha: 1.0)
                
                if(isUnitStarted == true)
                {
                    UnitOneIndex = 1
                    isUnitSecond = false
                    isUnitOne = true
                    cell.startUnitLbl.setTitle("RE-START UNIT", forState: .Normal)
                }
            }
            if(indexPath.row > 1)
            {
                cell.signOffCover.hidden = true
                cell.startUnitLbl.backgroundColor = UIColor.darkGrayColor()
                cell.unitView.backgroundColor = UIColor.lightGrayColor()
                cell.unitNumber.backgroundColor = UIColor.darkGrayColor()
                cell.startOff.hidden = true
                cell.yellowUnitAbout.hidden = true
                cell.startUnitLbl.enabled = false
                
                if(indexPath.row == 2)
                {
                    
                    if(isUnitStarted == true)
                    {
                        UnitOneIndex = 2
                        isUnitSecond = true
                        isUnitOne = false
                        cell.startUnitLbl.setTitle("START UNIT ", forState: .Normal)
                        cell.unitView.backgroundColor = UIColor(red: 252.0/255.0, green: 61.0/255.0, blue: 61.0/255.0, alpha: 1.0)
                        cell.startUnitLbl.backgroundColor = UIColor(red: 26.0/255.0, green: 94.0/255.0, blue: 139.0/255.0, alpha: 1.0)
                        cell.unitNumber.backgroundColor = UIColor(red: 26.0/255.0, green: 94.0/255.0, blue: 139.0/255.0, alpha: 1.0)
                        cell.startUnitLbl.enabled = true
                    }
                }
                
            }
            if(indexPath.row == 5)
            {
                cell.signOffCover.hidden = false
                cell.unitAbout.font = UIFont(name:"HelveticaNeue-Bold", size: 11.0)
                cell.yellowUnitAbout.font = UIFont(name:"HelveticaNeue-Bold", size: 11.0)
                
                cell.startUnitCoverView.hidden = true
                cell.whiteRedView.hidden = true
                cell.unitView.layer.borderColor = UIColor.clearColor().CGColor
                
                cell.unitAbout.text =  unitArray[indexPath.row].valueForKey("unitAbout") as? String
//                cell.unitAbout.font =  cell.unitAbout.font.fontWithSize(9)
                cell.unitAbout.numberOfLines = 15
                cell.unitView.backgroundColor = UIColor.clearColor()
                cell.unitNumber.hidden = true
                cell.startUnitLbl.hidden = true
                cell.startOff.hidden = false
                cell.yellowUnitAbout.hidden = true
                cell.startUnitLbl.enabled = false
            }
        }
        
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
        // Cell.separatorInset = UIEdgeInsetsZero;
        tableView.separatorInset = UIEdgeInsetsZero;
        tableView.separatorColor = UIColor.clearColor()
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {

    }
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func redoBtn(sender: AnyObject)
    {
        let SCQuestionsz = self.storyboard!.instantiateViewControllerWithIdentifier("SCQuestions") as! SCQuestions
        self.navigationController?.pushViewController(SCQuestionsz, animated: true)
    }
    @IBAction func startUnitBtn(sender: AnyObject)
    {
        if(sender.titleLabel!!.text == "RE-START UNIT")
        {
            let SCVideoz = self.storyboard!.instantiateViewControllerWithIdentifier("SCVideo") as! SCVideo
            self.navigationController?.pushViewController(SCVideoz, animated: true)
        }
        else if(sender.titleLabel!!.text == "START UNIT ")
        {
            let SCStartQuizz = self.storyboard!.instantiateViewControllerWithIdentifier("SCStartQuiz") as! SCStartQuiz
            self.navigationController?.pushViewController(SCStartQuizz, animated: true)
        }
        else if(sender.titleLabel!!.text == " START UNIT "||sender.titleLabel!!.text == " RE-START UNIT ")
        {
            let SCDownloadHandbookz = self.storyboard!.instantiateViewControllerWithIdentifier("SCDownloadHandbook") as! SCDownloadHandbook
            self.navigationController?.pushViewController(SCDownloadHandbookz, animated: true)
        }
        else if(sender.titleLabel!!.text == "  START UNIT  ")
        {
            let SCUnitFourz = self.storyboard!.instantiateViewControllerWithIdentifier("SCUnitFour") as! SCUnitFour
            self.navigationController?.pushViewController(SCUnitFourz, animated: true)
        }
        else
        {
            let SCVideoz = self.storyboard!.instantiateViewControllerWithIdentifier("SCVideo") as! SCVideo
            self.navigationController?.pushViewController(SCVideoz, animated: true)
        }

    }
    @IBAction func signOffBtn(sender: AnyObject)
    {
        let SCSignOffCoursez = self.storyboard!.instantiateViewControllerWithIdentifier("SCSignOffCourse") as! SCSignOffCourse
        self.navigationController?.pushViewController(SCSignOffCoursez, animated: true)
    }
}
