//
//  SCMyCourses.swift
//  SocialCare
//
//  Created by Mrinal Khullar on 4/26/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class SCMyCourses: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0
    
    @IBOutlet weak var myCoursesOutlet: UIButton!
    @IBOutlet weak var resultsOutlet: UIButton!
    @IBOutlet weak var statsOutlet: UIButton!
    @IBOutlet weak var mycourseTableView: UITableView!
    
    ///////////////////////////////////////
    
    var mycourseArray:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        fillMyCourseLisitng()
        mycourseTableView.backgroundColor = UIColor.clearColor()
        
         myCoursesOutlet!.backgroundColor = UIColor(patternImage: UIImage(named: "red.png")!)
        resultsOutlet!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        statsOutlet!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool)
    {
        closeMenuBtn.hidden = true
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fillMyCourseLisitng()
    {
        mycourseArray .addObject(["courseTitle":"ADOPTION FOSTERING","CourseImage":"image3.jpg","courseReview":"( 199 REVIEWS )","courseDetails":"LEARNING OUTCOMES WILL INCLUDE PARTICIPANTS BEING ABLE TO: UNDERSTAND THE CONCEPT OF PERMANANCE IN THE CONTEXT OF CARE PLANNING AND..."])
        mycourseArray .addObject(["courseTitle":"APPROPRIATE ADULT CHILDREN'S HOME","CourseImage":"image4.jpeg","courseReview":"( 199 REVIEWS )","courseDetails":"LEARNING OUTCOMES WILL INCLUDE PARTICIPANTS BEING ABLE TO: UNDERSTAND THE CONCEPT OF PERMANANCE IN THE CONTEXT OF CARE PLANNING AND..."])
        mycourseArray .addObject(["courseTitle":"APPROPRIATE ADULT FOSTERING","CourseImage":"image3.jpg","courseReview":"( 199 REVIEWS )","courseDetails":"LEARNING OUTCOMES WILL INCLUDE PARTICIPANTS BEING ABLE TO: UNDERSTAND THE CONCEPT OF PERMANANCE IN THE CONTEXT OF CARE PLANNING AND..."])
        // print("locationListingArray is\(locationListingArray)")
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 200.0
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1;
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return mycourseArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("SCCustomCell", forIndexPath: indexPath) as! SCCustomCell
        
        
        cell.courseCellView.layer.cornerRadius = 8
        cell.courseCellView.layer.borderWidth = 3
        cell.courseCellView.layer.borderColor = UIColor(red: 70.0/255.0, green: 139.0/255.0, blue: 185.0/255.0, alpha: 1.0).CGColor
        
        cell.startCourseLbl.layer.cornerRadius = 6
        cell.startCourseLbl.layer.borderWidth = 3
        cell.startCourseLbl.layer.borderColor = UIColor.clearColor().CGColor
        
        cell.coursecompletedLbl.layer.cornerRadius = 6
        cell.coursecompletedLbl.layer.borderWidth = 3
        cell.coursecompletedLbl.layer.borderColor = UIColor.clearColor().CGColor
        
        cell.handbookLbl.layer.cornerRadius = 6
        cell.handbookLbl.layer.borderWidth = 3
        cell.handbookLbl.layer.borderColor = UIColor.clearColor().CGColor
        
        cell.courseDescription.textColor = UIColor.whiteColor()

        
        cell.courseTitle.text = mycourseArray[indexPath.row].valueForKey("courseTitle") as? String
        let courseImages = mycourseArray[indexPath.row].valueForKey("CourseImage") as? String
        cell.courseImageView.image = UIImage(named:courseImages!)
        cell.courseReviews.text = mycourseArray[indexPath.row].valueForKey("courseReview") as? String
        cell.courseDescription.text = mycourseArray[indexPath.row].valueForKey("courseDetails") as? String
        cell.courseDescription.textColor = UIColor.whiteColor()
        cell.courseDescription.font = UIFont(name:"HelveticaNeue", size: 11.0)
        
        
        cell.startCourseLbl.tag = indexPath.row+121
        
        if(indexPath.row == 1)
        {
            cell.startCourseLbl.hidden = true
        }
        
        
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
        // Cell.separatorInset = UIEdgeInsetsZero;
        tableView.separatorInset = UIEdgeInsetsZero;
        tableView.separatorColor = UIColor.clearColor()
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {

        
    }

    @IBAction func startCourseBtn(sender: AnyObject)
    {
        let SCStartCourseZ = self.storyboard!.instantiateViewControllerWithIdentifier("SCStartCourse") as! SCStartCourse
        
        self.navigationController?.pushViewController(SCStartCourseZ, animated: true)
    }
    @IBAction func courseCompletedBtn(sender: AnyObject)
    {
        
    }
    
    @IBAction func statsBtn(sender: AnyObject)
    {
        let SCStatsz = self.storyboard!.instantiateViewControllerWithIdentifier("SCStats") as! SCStats
        
        self.navigationController?.pushViewController(SCStatsz, animated: false)
    }
    @IBAction func hanbookBtn(sender: AnyObject)
    {
    }
    @IBAction func menuBtn(sender: AnyObject)
    {
        myMenuView.hidden = false
        
        print("-myMenuView.frame.size.width and x is \(-myMenuView.frame.size.width)\(x)")
        
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
        else
        {
            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.5, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })
    }
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
}
