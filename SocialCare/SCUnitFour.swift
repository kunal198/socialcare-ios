
//
//  SCUnitFour.swift
//  SocialCare
//
//  Created by Mrinal Khullar on 4/27/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class SCUnitFour: UIViewController,UITextFieldDelegate
{
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0

    @IBOutlet weak var submitOutlet: UIButton!
    @IBOutlet weak var submitView: UIView!
    @IBOutlet weak var stateTxtField: UITextField!
    @IBOutlet weak var nameThirdTxtfield: UITextField!
    @IBOutlet weak var nameSecondTxtFeild: UITextField!
    @IBOutlet weak var nametxtField: UITextField!
    @IBOutlet weak var blueCoverView: UIView!
    @IBOutlet weak var blueView: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        submitOutlet.layer.cornerRadius = 8
        submitOutlet.layer.borderWidth = 4
        submitOutlet.layer.borderColor = UIColor.clearColor().CGColor
        
        submitView.layer.cornerRadius = 8
        submitView.layer.borderWidth = 4
        submitView.layer.borderColor = UIColor.clearColor().CGColor
        
        blueCoverView.layer.cornerRadius = 8
        blueCoverView.layer.borderWidth = 4
        blueCoverView.layer.borderColor = UIColor.clearColor().CGColor
        
        blueView.layer.cornerRadius = 8
        blueView.layer.borderWidth = 4
        blueView.layer.borderColor = UIColor.clearColor().CGColor
        
        placeHolderMethodForUnitFour()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool)
    {
         closeMenuBtn.hidden = true
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menuBtn(sender: AnyObject)
    {
        myMenuView.hidden = false
        
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
        else
        {
            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.5, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })
    }
    func placeHolderMethodForUnitFour()
    {
        stateTxtField.layer.cornerRadius = 8
        stateTxtField.layer.borderWidth = 4
        stateTxtField.layer.borderColor = UIColor.clearColor().CGColor
        
        nameThirdTxtfield.layer.cornerRadius = 8
        nameThirdTxtfield.layer.borderWidth = 4
        nameThirdTxtfield.layer.borderColor = UIColor.clearColor().CGColor
        
        nameSecondTxtFeild.layer.cornerRadius = 8
        nameSecondTxtFeild.layer.borderWidth = 4
        nameSecondTxtFeild.layer.borderColor = UIColor.clearColor().CGColor
        
        nametxtField.layer.cornerRadius = 8
        nametxtField.layer.borderWidth = 4
        nametxtField.layer.borderColor = UIColor.clearColor().CGColor
        
        let paddingView = UIView(frame: CGRectMake(0, 0, 5, stateTxtField.frame.height))
        stateTxtField.leftView = paddingView
        stateTxtField.leftViewMode = UITextFieldViewMode.Always
        
        stateTxtField.attributedPlaceholder = NSAttributedString(string:"STATE HOW YOU WILL IMPLEMENT THE ABOVE THREE THINGS", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 10)!])

        
        let paddingViewnameSecondTxtFeild = UIView(frame: CGRectMake(0, 0, 5, nameSecondTxtFeild.frame.height))
        nameSecondTxtFeild.leftView = paddingViewnameSecondTxtFeild
        nameSecondTxtFeild.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewPwdnameSecondTxtFeild = UIView(frame: CGRectMake(0, 0, 5, nameThirdTxtfield.frame.height))
        nameThirdTxtfield.leftView = paddingViewPwdnameSecondTxtFeild
        nameThirdTxtfield.leftViewMode = UITextFieldViewMode.Always
        
        
        nameSecondTxtFeild.attributedPlaceholder = NSAttributedString(string:"NAME SECOND THING LEARNT", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 10)!])
        nameThirdTxtfield.attributedPlaceholder = NSAttributedString(string:"NAME THIRD THING LEARNT", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 10)!])
        
        
        let paddingViewnametxtField = UIView(frame: CGRectMake(0, 0, 5, nametxtField.frame.height))
        nametxtField.leftView = paddingViewnametxtField
        nametxtField.leftViewMode = UITextFieldViewMode.Always
        
        
        nametxtField.attributedPlaceholder = NSAttributedString(string:"NAME OF THE THING YOU LEARNT FROM THE COURSE", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 10)!])
    }
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func submitBtn(sender: AnyObject)
    {
        let SCStartCoursez = self.storyboard!.instantiateViewControllerWithIdentifier("SCStartCourse") as! SCStartCourse
        self.navigationController?.pushViewController(SCStartCoursez, animated: true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        stateTxtField.resignFirstResponder()
        nameThirdTxtfield.resignFirstResponder()
        nametxtField.resignFirstResponder()
        nameSecondTxtFeild.resignFirstResponder()
        
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        stateTxtField.resignFirstResponder()
        nameThirdTxtfield.resignFirstResponder()
        nametxtField.resignFirstResponder()
        nameSecondTxtFeild.resignFirstResponder()
    }

}
