//
//  AppDelegate.swift
//  SocialCare
//
//  Created by Mrinal Khullar on 4/25/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

var x: CGFloat = 0.0
var urlLink = String()

var navBarTitle = String()

var currentVC:UIViewController = UIViewController()

var postPropertyVC: UIViewController = UIViewController()

var myMenuView :UIView = UIView()
var courseView :UIView?
var requestView :UIView?
var accessCertificateView :UIView?
var editAccountsView :UIView?
var activityView :UIView?
var profileView :UIView?
var messageView :UIView?
var logoutView :UIView?
var usernameView :UIView?
var profileImageView :UIView?

var closeMenuBtn:UIButton = UIButton()
let courseBtn:UIButton = UIButton()
let requestBtn:UIButton = UIButton()
let accessCertificateBtn:UIButton = UIButton()
let editAccountsBtn:UIButton = UIButton()
let activityBtn:UIButton = UIButton()
let profileBtn:UIButton = UIButton()
let messageBtn:UIButton = UIButton()
let logoutBtn:UIButton = UIButton()
let usernameBtn:UIButton = UIButton()
let profileImageBtn:UIButton = UIButton()

var barButtonCheck = String()
/////////

let dashBoardLAbel:UILabel = UILabel()
let courseLabel:UILabel = UILabel()
let requestLabel:UILabel = UILabel()
let accessCertificateLabel:UILabel = UILabel()
let editAccountsLabel:UILabel = UILabel()
let activityLabel:UILabel = UILabel()
let profileLabel:UILabel = UILabel()
let messageLabel:UILabel = UILabel()
let logoutLabel:UILabel = UILabel()
let usernameLabel:UILabel = UILabel()
let profileImageLabel:UILabel = UILabel()


var profileImage : UIImageView = UIImageView()

var dashBoardImage : UIImageView = UIImageView()
var courseImage : UIImageView = UIImageView()
var requestImage : UIImageView = UIImageView()
var accessCertificateImage : UIImageView = UIImageView()
var editAccountsImage : UIImageView = UIImageView()
var activityImage : UIImageView = UIImageView()
var messageImage : UIImageView = UIImageView()
var logoutImg : UIImageView = UIImageView()


/////////

let dashBoard_btn:UIButton = UIButton()

var isMenuSelected = false

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{

    var window: UIWindow?
    
    var menu_Label:UILabel = UILabel()
    
    let lineImage: UIImageView = UIImageView()
    
    //MARK: 1.DashBoard View Variables
    var dashBoardView :UIView = UIView()
    var dashBoardView_line:UILabel = UILabel()
    
    
    
    func creatingMenu()
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            myMenuView = UIView(frame:CGRectMake(0, 64,(250/320)*UIScreen.mainScreen().bounds.size.width, (780/568)*UIScreen.mainScreen().bounds.size.height))
            
        }
        else
        {
            if (UIScreen.mainScreen().bounds.size.height == 568)
            {
                myMenuView = UIView(frame:CGRectMake(0, 0, 175, 568))
            }
                
            else if(UIScreen.mainScreen().bounds.size.height == 480)
            {
                myMenuView = UIView(frame: CGRectMake(0, 0, 250, 480))
            }
                
            else if(UIScreen.mainScreen().bounds.size.height == 736)
            {
                myMenuView = UIView(frame: CGRectMake(0, 0, 238, 736))
            }
            else if(UIScreen.mainScreen().bounds.size.height == 667)
            {
                myMenuView = UIView(frame: CGRectMake(0, 0, 218, 667))
            }
            else
            {
                myMenuView = UIView(frame:CGRectMake(0, 0,320 , 620))
            }
        }
        
        closeMenuBtn.frame = CGRectMake(0,0,UIScreen.mainScreen().bounds.size.width,UIScreen.mainScreen().bounds.size.height)
        closeMenuBtn.backgroundColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.9)
        closeMenuBtn.alpha = 0.5
        closeMenuBtn.hidden = true
        
        window?.addSubview(closeMenuBtn)
        window?.addSubview(myMenuView)
        
        myMenuView.backgroundColor = UIColor(red: 209.0/255.0, green: 209.0/255.0, blue: 209.0/255.0, alpha: 1.0)
        myMenuView.backgroundColor = UIColor(patternImage: UIImage(named: "grey.png")!)

        closeMenuBtn.addTarget(self, action:"closeMenuBtnClicked:",forControlEvents: UIControlEvents.TouchUpInside)
        
        if(UIScreen.mainScreen().bounds.size.height == 667)
        {
            dashBoardView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (0/568)*UIScreen.mainScreen().bounds.size.height, (164/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
        }
        else if(UIScreen.mainScreen().bounds.size.height == 736)
        {
            
            dashBoardView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (239/568)*UIScreen.mainScreen().bounds.size.height, (184/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
            
//            dashBoardView.backgroundColor =  UIColor.whiteColor()
        }
        else if(UIScreen.mainScreen().bounds.size.height == 568)
        {
            dashBoardView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (0/568)*UIScreen.mainScreen().bounds.size.height, (184/320)*UIScreen.mainScreen().bounds.size.width, (50/568)*UIScreen.mainScreen().bounds.size.height))
            
//            dashBoardView.backgroundColor =  UIColor.whiteColor()
        }
        else
        {
            dashBoardView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (0/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (65/568)*UIScreen.mainScreen().bounds.size.height))
            
//            dashBoardView.backgroundColor =  UIColor.whiteColor()
        }
        myMenuView .addSubview(dashBoardView)
        
//        dashBoardImage = UIImageView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (239/568)*UIScreen.mainScreen().bounds.size.height, (170/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
//        myMenuView.addSubview(dashBoardImage)
        
        
        dashBoardLAbel.frame = CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width,(240/568)*UIScreen.mainScreen().bounds.size.height,(150/414)*UIScreen.mainScreen().bounds.size.width,(33/568)*UIScreen.mainScreen().bounds.size.height)
        
        
        
        dashBoardLAbel.textColor = UIColor.blackColor()
        dashBoardLAbel.textAlignment = NSTextAlignment.Left
        dashBoardLAbel.text = "DASHBOARD"
//        dashBoardLAbel.backgroundColor = UIColor.greenColor()
        if (UIScreen.mainScreen().bounds.size.height == 568)
        {
             dashBoardLAbel.font = UIFont(name:"HelveticaNeue", size: 12.0)
        }
        else
        {
            dashBoardLAbel.font = UIFont(name:"HelveticaNeue", size: 14.0)
        }

        
        dashBoardLAbel.tag = 0
        
        myMenuView .addSubview(dashBoardLAbel)
        
        
        dashBoard_btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(170/320)*UIScreen.mainScreen().bounds.size.width,(33/568)*UIScreen.mainScreen().bounds.size.height)
        
//        dashBoard_btn.backgroundColor = UIColor.blueColor()
        dashBoard_btn.setTitle ("", forState: UIControlState.Normal)
        dashBoard_btn.addTarget(self, action:"Home:",forControlEvents: UIControlEvents.TouchUpInside)
        
        dashBoardView.addSubview(dashBoard_btn)
        
        dashBoard_btn.tag = 0
        
        //////////////////////////////////////////////
        
        if(UIScreen.mainScreen().bounds.size.height == 667)
        {
            courseView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (274/568)*UIScreen.mainScreen().bounds.size.height, (185/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
        }
        else if(UIScreen.mainScreen().bounds.size.height == 736)
        {
            courseView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (274/568)*UIScreen.mainScreen().bounds.size.height, (184/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
        }
        else
        {
            courseView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (274/568)*UIScreen.mainScreen().bounds.size.height, (184/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
        }

        
//        courseImage = UIImageView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (274/568)*UIScreen.mainScreen().bounds.size.height, (170/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
//        myMenuView.addSubview(courseImage)
        
//        courseView!.backgroundColor = UIColor.lightGrayColor()
        
        myMenuView.addSubview(courseView!)
        
        courseLabel.frame = CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(150/414)*UIScreen.mainScreen().bounds.size.width,(33/568)*UIScreen.mainScreen().bounds.size.height)
        
        //newEssayLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        courseLabel.textColor = UIColor.blackColor()
//        courseLabel.backgroundColor = UIColor.greenColor()
        courseLabel.textAlignment = NSTextAlignment.Left
        courseLabel.text = "MY COURSES"
        courseLabel.font = UIFont(name:"HelveticaNeue", size: 14.0)
        courseLabel.tag = 1
        
        
        courseBtn.addSubview(courseLabel)

        courseBtn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(170/320)*UIScreen.mainScreen().bounds.size.width,(33/568)*UIScreen.mainScreen().bounds.size.height)
        
//        courseBtn.backgroundColor = UIColor.yellowColor()
        courseBtn.setTitle ("", forState: UIControlState.Normal)
        courseBtn.addTarget(self, action:"goTocourseBtn:",forControlEvents: UIControlEvents.TouchUpInside)
        
        // courseBtn.backgroundColor = UIColor.blackColor()

        courseView!.tag = 95
        
        courseView?.addSubview(courseBtn)
        
        courseBtn.tag = 1
        
        /////////////////////////REQUEST A COURSE ///////////////////////
        
        if(UIScreen.mainScreen().bounds.size.height == 667)
        {
            requestView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (308/568)*UIScreen.mainScreen().bounds.size.height, (185/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
        }
        else if(UIScreen.mainScreen().bounds.size.height == 736)
        {
            requestView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (308/568)*UIScreen.mainScreen().bounds.size.height, (184/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
        }
        else
        {
            requestView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (308/568)*UIScreen.mainScreen().bounds.size.height, (184/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
        }


//        requestImage = UIImageView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (308/568)*UIScreen.mainScreen().bounds.size.height, (170/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
//        myMenuView.addSubview(requestImage)
        
        
//        requestView!.backgroundColor = UIColor.lightGrayColor()
        
        myMenuView.addSubview(requestView!)
        
//        if(UIScreen.mainScreen().bounds.size.height == 667)
//        {
//            requestLabel.frame = CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(170/414)*UIScreen.mainScreen().bounds.size.width,(33/568)*UIScreen.mainScreen().bounds.size.height)
//        }
//        else
//        {
            requestLabel.frame = CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(160/414)*UIScreen.mainScreen().bounds.size.width,(33/568)*UIScreen.mainScreen().bounds.size.height)
      //  }
        
        //newEssayLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        requestLabel.textColor = UIColor.blackColor()
//        requestLabel.backgroundColor = UIColor.greenColor()
        requestLabel.textAlignment = NSTextAlignment.Left
        requestLabel.text = "REQUEST A COURSE"
        requestLabel.font = UIFont(name:"HelveticaNeue", size: 14.0)
        requestLabel.tag = 2
        
        
        requestBtn.addSubview(requestLabel)
        
        requestBtn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(170/320)*UIScreen.mainScreen().bounds.size.width,(33/568)*UIScreen.mainScreen().bounds.size.height)
        
//        requestBtn.backgroundColor = UIColor.yellowColor()
        requestBtn.setTitle ("", forState: UIControlState.Normal)
        requestBtn.addTarget(self, action:"goTorequestBtn:",forControlEvents: UIControlEvents.TouchUpInside)
        
        // courseBtn.backgroundColor = UIColor.blackColor()
        
        requestView!.tag = 95
        
        requestView?.addSubview(requestBtn)
        
        requestBtn.tag = 2
        
        //////////////////////////////////////////////
        
        /////////////////////////ACCESS CERTIFICATES///////////////////////
        
        if(UIScreen.mainScreen().bounds.size.height == 667)
        {
            accessCertificateView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (342/568)*UIScreen.mainScreen().bounds.size.height, (185/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
        }
        else if(UIScreen.mainScreen().bounds.size.height == 736)
        {
            accessCertificateView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (342/568)*UIScreen.mainScreen().bounds.size.height, (184/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
        }
        else
        {
            accessCertificateView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (342/568)*UIScreen.mainScreen().bounds.size.height, (184/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
        }
        

        
//        accessCertificateImage = UIImageView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (342/568)*UIScreen.mainScreen().bounds.size.height, (170/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
//        myMenuView.addSubview(accessCertificateImage)
        
//        accessCertificateView!.backgroundColor = UIColor.lightGrayColor()
        
        myMenuView.addSubview(accessCertificateView!)
        
        accessCertificateLabel.frame = CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(200/414)*UIScreen.mainScreen().bounds.size.width,(33/568)*UIScreen.mainScreen().bounds.size.height)
        
        //newEssayLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        accessCertificateLabel.textColor = UIColor.blackColor()
//        accessCertificateLabel.backgroundColor = UIColor.greenColor()
        accessCertificateLabel.textAlignment = NSTextAlignment.Left
        accessCertificateLabel.text = "ACCESS CERTIFICATES"
        accessCertificateLabel.font = UIFont(name:"HelveticaNeue", size: 14.0)
        accessCertificateLabel.tag = 3
        
        
        accessCertificateBtn.addSubview(accessCertificateLabel)
        
        accessCertificateBtn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(170/320)*UIScreen.mainScreen().bounds.size.width,(33/568)*UIScreen.mainScreen().bounds.size.height)
        
//        accessCertificateBtn.backgroundColor = UIColor.yellowColor()
        accessCertificateBtn.setTitle ("", forState: UIControlState.Normal)
        accessCertificateBtn.addTarget(self, action:"goToaccessCertificateBtn:",forControlEvents: UIControlEvents.TouchUpInside)
        
        // courseBtn.backgroundColor = UIColor.blackColor()
        
        accessCertificateView!.tag = 95
        
        accessCertificateView?.addSubview(accessCertificateBtn)

        accessCertificateBtn.tag = 3
        
        //////////////////////////////////////////////
        
        /////////////////////////EDIT ACCOUNT DETAILS///////////////////////
        
        if(UIScreen.mainScreen().bounds.size.height == 667)
        {
            editAccountsView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (376/568)*UIScreen.mainScreen().bounds.size.height, (185/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
        }
        else if(UIScreen.mainScreen().bounds.size.height == 736)
        {
            editAccountsView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (376/568)*UIScreen.mainScreen().bounds.size.height, (184/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
        }
        else
        {
            editAccountsView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (376/568)*UIScreen.mainScreen().bounds.size.height, (184/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
        }

        
        
//        editAccountsImage = UIImageView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (376/568)*UIScreen.mainScreen().bounds.size.height, (170/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
//        myMenuView.addSubview(editAccountsImage)
        
        
//        editAccountsView!.backgroundColor = UIColor.lightGrayColor()
        
        myMenuView.addSubview(editAccountsView!)
        
        editAccountsLabel.frame = CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(200/414)*UIScreen.mainScreen().bounds.size.width,(33/568)*UIScreen.mainScreen().bounds.size.height)
        
        //newEssayLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        editAccountsLabel.textColor = UIColor.blackColor()
//        editAccountsLabel.backgroundColor = UIColor.greenColor()
        editAccountsLabel.textAlignment = NSTextAlignment.Left
        editAccountsLabel.text = "EDIT ACCOUNT DETAILS"
        editAccountsLabel.font = UIFont(name:"HelveticaNeue", size: 14.0)
        editAccountsLabel.tag = 4
        
        
        editAccountsBtn.addSubview(editAccountsLabel)
        
        editAccountsBtn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(170/320)*UIScreen.mainScreen().bounds.size.width,(33/568)*UIScreen.mainScreen().bounds.size.height)
        
//        editAccountsBtn.backgroundColor = UIColor.yellowColor()
        editAccountsBtn.setTitle ("", forState: UIControlState.Normal)
        editAccountsBtn.addTarget(self, action:"goToeditAccountsBtn:",forControlEvents: UIControlEvents.TouchUpInside)
        
        // courseBtn.backgroundColor = UIColor.blackColor()
        
        editAccountsView!.tag = 95
        
        editAccountsView?.addSubview(editAccountsBtn)
        
        editAccountsBtn.tag = 4
        
        //////////////////////////////////////////////
        
        /////////////////////////ACTIVITY///////////////////////
        
        if(UIScreen.mainScreen().bounds.size.height == 667)
        {
            activityView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (410/568)*UIScreen.mainScreen().bounds.size.height, (185/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
        }
        else if(UIScreen.mainScreen().bounds.size.height == 736)
        {
            activityView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (410/568)*UIScreen.mainScreen().bounds.size.height, (184/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
        }
        else
        {
            activityView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (410/568)*UIScreen.mainScreen().bounds.size.height, (184/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
        }

        
//        activityImage = UIImageView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (410/568)*UIScreen.mainScreen().bounds.size.height, (170/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
//        myMenuView.addSubview(activityImage)
        
//        activityView!.backgroundColor = UIColor.lightGrayColor()
        
        myMenuView.addSubview(activityView!)
        
        activityLabel.frame = CGRectMake((16/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(200/414)*UIScreen.mainScreen().bounds.size.width,(33/568)*UIScreen.mainScreen().bounds.size.height)
        
        //newEssayLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        activityLabel.textColor = UIColor.blackColor()
//        activityLabel.backgroundColor = UIColor.greenColor()
        activityLabel.textAlignment = NSTextAlignment.Left
        activityLabel.text = "ACTIVITY"
        activityLabel.font = UIFont(name:"HelveticaNeue", size: 14.0)
        activityLabel.tag = 5
        
        
        activityBtn.addSubview(activityLabel)
        
        activityBtn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(170/320)*UIScreen.mainScreen().bounds.size.width,(33/568)*UIScreen.mainScreen().bounds.size.height)
        
//        activityBtn.backgroundColor = UIColor.yellowColor()
        activityBtn.setTitle ("", forState: UIControlState.Normal)
        activityBtn.addTarget(self, action:"goToactivityBtn:",forControlEvents: UIControlEvents.TouchUpInside)
        
        // courseBtn.backgroundColor = UIColor.blackColor()
        
        activityView!.tag = 95
        
        activityView?.addSubview(activityBtn)
        
        activityBtn.tag = 5
        
        //////////////////////////////////////////////
        
        /////////////////////////PROFILE///////////////////////
        
         if(UIScreen.mainScreen().bounds.size.height == 667)
         {
            profileView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (444/568)*UIScreen.mainScreen().bounds.size.height, (185/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
         }
         else if(UIScreen.mainScreen().bounds.size.height == 736)
         {
            profileView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (444/568)*UIScreen.mainScreen().bounds.size.height, (184/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
         }
         else
         {
            profileView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (444/568)*UIScreen.mainScreen().bounds.size.height, (184/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
         }

        
//        profileImg = UIImageView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (444/568)*UIScreen.mainScreen().bounds.size.height, (170/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
//        myMenuView.addSubview(profileImg)
        
        
//        profileView!.backgroundColor = UIColor.lightGrayColor()
        
        myMenuView.addSubview(profileView!)
        
        profileLabel.frame = CGRectMake((16/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(200/414)*UIScreen.mainScreen().bounds.size.width,(33/568)*UIScreen.mainScreen().bounds.size.height)
        
        //newEssayLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        profileLabel.textColor = UIColor.blackColor()
//        profileLabel.backgroundColor = UIColor.greenColor()
        profileLabel.textAlignment = NSTextAlignment.Left
        profileLabel.text = "PROFILE"
        profileLabel.font = UIFont(name:"HelveticaNeue", size: 14.0)
        profileLabel.tag = 6
        
        
        profileBtn.addSubview(profileLabel)
        
        profileBtn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(170/320)*UIScreen.mainScreen().bounds.size.width,(33/568)*UIScreen.mainScreen().bounds.size.height)
        
//        profileBtn.backgroundColor = UIColor.yellowColor()
        profileBtn.setTitle ("", forState: UIControlState.Normal)
        profileBtn.addTarget(self, action:"goToprofileBtn:",forControlEvents: UIControlEvents.TouchUpInside)
        
        // courseBtn.backgroundColor = UIColor.blackColor()
        
        profileView!.tag = 95
        
        profileView?.addSubview(profileBtn)
        
        profileBtn.tag = 6
        
        //////////////////////////////////////////////
        
        /////////////////////////MESSAGES///////////////////////
        
        if(UIScreen.mainScreen().bounds.size.height == 667)
        {
            messageView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (480/568)*UIScreen.mainScreen().bounds.size.height, (185/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
        }
        else if(UIScreen.mainScreen().bounds.size.height == 736)
        {
            messageView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (480/568)*UIScreen.mainScreen().bounds.size.height, (184/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
        }
        else
        {
            messageView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (480/568)*UIScreen.mainScreen().bounds.size.height, (184/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
        }

        
        
//        messageImage = UIImageView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (480/568)*UIScreen.mainScreen().bounds.size.height, (170/320)*UIScreen.mainScreen().bounds.size.width, (33/568)*UIScreen.mainScreen().bounds.size.height))
//        myMenuView.addSubview(messageImage)
//        
//        messageView!.backgroundColor = UIColor.lightGrayColor()
        
        myMenuView.addSubview(messageView!)
        
        messageLabel.frame = CGRectMake((16/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(200/414)*UIScreen.mainScreen().bounds.size.width,(33/568)*UIScreen.mainScreen().bounds.size.height)
        
        //newEssayLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        messageLabel.textColor = UIColor.blackColor()
//        messageLabel.backgroundColor = UIColor.greenColor()
        messageLabel.textAlignment = NSTextAlignment.Left
        messageLabel.text = "MESSAGES"
        messageLabel.font = UIFont(name:"HelveticaNeue", size: 14.0)
        messageLabel.tag = 7
        
        
        messageBtn.addSubview(messageLabel)
        
        messageBtn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(170/320)*UIScreen.mainScreen().bounds.size.width,(33/568)*UIScreen.mainScreen().bounds.size.height)
        
//        messageBtn.backgroundColor = UIColor.yellowColor()
        messageBtn.setTitle ("", forState: UIControlState.Normal)
        messageBtn.addTarget(self, action:"goTomessageBtn:",forControlEvents: UIControlEvents.TouchUpInside)
        
        // courseBtn.backgroundColor = UIColor.blackColor()
        
        messageView!.tag = 95
        
        messageView?.addSubview(messageBtn)
        
        messageBtn.tag = 7
        
        //////////////////////////////////////////////
        
        /////////////////////////LOG OUT///////////////////////
        
        if (UIScreen.mainScreen().bounds.size.height == 568)
        {
            logoutView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (534/568)*UIScreen.mainScreen().bounds.size.height, (177/320)*UIScreen.mainScreen().bounds.size.width, (34/568)*UIScreen.mainScreen().bounds.size.height))
        }
        else
        {
            logoutView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (534/568)*UIScreen.mainScreen().bounds.size.height, (185/320)*UIScreen.mainScreen().bounds.size.width, (34/568)*UIScreen.mainScreen().bounds.size.height))
        }

        
        logoutView!.backgroundColor = UIColor(red: 252/255, green: 61/255, blue:61/255, alpha: 1)
        
        myMenuView.addSubview(logoutView!)
        
        logoutLabel.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(200/414)*UIScreen.mainScreen().bounds.size.width,(34/568)*UIScreen.mainScreen().bounds.size.height)
        
        logoutView!.backgroundColor = UIColor(patternImage: UIImage(named: "red.png")!)
        
        //newEssayLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        logoutLabel.textColor = UIColor.blackColor()
//        logoutLabel.backgroundColor = UIColor.greenColor()
        logoutLabel.textAlignment = NSTextAlignment.Center
        logoutLabel.text = "LOG OUT"
        logoutLabel.font = UIFont(name:"HelveticaNeue", size: 14.0)
        logoutLabel.tag = 8
        
        
        logoutBtn.addSubview(logoutLabel)
        
        logoutBtn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(170/320)*UIScreen.mainScreen().bounds.size.width,(33/568)*UIScreen.mainScreen().bounds.size.height)
        
//        logoutBtn.backgroundColor = UIColor.yellowColor()
        logoutBtn.setTitle ("", forState: UIControlState.Normal)
        logoutBtn.addTarget(self, action:"goTologoutBtn:",forControlEvents: UIControlEvents.TouchUpInside)
        
        // courseBtn.backgroundColor = UIColor.blackColor()
        
        logoutView!.tag = 95
        
        logoutView?.addSubview(logoutBtn)
        
        logoutBtn.tag = 8
        
        //////////////////////////////////////////////
        
        /////////////////////////USER NAME///////////////////////
        
        if (UIScreen.mainScreen().bounds.size.height == 568)
        {
            usernameView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (198/568)*UIScreen.mainScreen().bounds.size.height, (177/320)*UIScreen.mainScreen().bounds.size.width, (41/568)*UIScreen.mainScreen().bounds.size.height))
        }
        else
        {
            usernameView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (198/568)*UIScreen.mainScreen().bounds.size.height, (185/320)*UIScreen.mainScreen().bounds.size.width, (41/568)*UIScreen.mainScreen().bounds.size.height))
        }
        
        usernameView!.backgroundColor = UIColor(red: 252/255, green: 61/255, blue:61/255, alpha:1.0)
        
        myMenuView.addSubview(usernameView!)
        
        usernameLabel.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(199/414)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        
        usernameView!.backgroundColor = UIColor(patternImage: UIImage(named: "red.png")!)
        
        //newEssayLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        usernameLabel.textColor = UIColor.whiteColor()
//        usernameLabel.backgroundColor = UIColor(red: 252/255, green: 61/255, blue:61/255, alpha:1.0)

        usernameLabel.textAlignment = NSTextAlignment.Center
        usernameLabel.text = "John's Doe"
        usernameLabel.font = UIFont(name:"HelveticaNeue", size: 18.0)
        usernameLabel.tag = 9
        
        
        usernameBtn.addSubview(usernameLabel)
        
        usernameBtn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(170/320)*UIScreen.mainScreen().bounds.size.width,(41/568)*UIScreen.mainScreen().bounds.size.height)
        
//        usernameBtn.backgroundColor = UIColor.yeellowColor()
        usernameBtn.setTitle ("", forState: UIControlState.Normal)
        usernameBtn.addTarget(self, action:"goTousernameBtn:",forControlEvents: UIControlEvents.TouchUpInside)
        
        // courseBtn.backgroundColor = UIColor.blackColor()
        
        usernameView!.tag = 95
        
        usernameView?.addSubview(usernameBtn)
        
        usernameBtn.tag = 9
        
        //////////////////////////////////////////////
        
        
        /////////////////////////profile view///////////////////////
        
        if (UIScreen.mainScreen().bounds.size.height == 568)
        {
            profileImageView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (0/568)*UIScreen.mainScreen().bounds.size.height, (185/320)*UIScreen.mainScreen().bounds.size.width, (198/568)*UIScreen.mainScreen().bounds.size.height))
        }
        else if(UIScreen.mainScreen().bounds.size.height == 667)
        {
            profileImageView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (0/568)*UIScreen.mainScreen().bounds.size.height, (185/320)*UIScreen.mainScreen().bounds.size.width, (198/568)*UIScreen.mainScreen().bounds.size.height))
        }
        else
        {
            profileImageView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (0/568)*UIScreen.mainScreen().bounds.size.height, (184/320)*UIScreen.mainScreen().bounds.size.width, (198/568)*UIScreen.mainScreen().bounds.size.height))
        }
        
        profileImageView!.backgroundColor = UIColor(patternImage: UIImage(named: "grdnt.png")!)
        
        myMenuView.addSubview(profileImageView!)
        
        profileImage = UIImageView(frame:CGRectMake((36/320)*UIScreen.mainScreen().bounds.size.width, (25/568)*UIScreen.mainScreen().bounds.size.height, (100/568)*UIScreen.mainScreen().bounds.size.height, (100/568)*UIScreen.mainScreen().bounds.size.height))
        
        profileImage.image = UIImage(named:"image3.jpg")
        
        profileImage.layer.borderWidth = 4
        profileImage.layer.cornerRadius =  profileImage.frame.size.width/2
        profileImage.layer.borderColor = UIColor(red: 241/255, green: 169/255, blue:191/255, alpha:1.0).CGColor
        profileImage.clipsToBounds = true
        profileImage.contentMode = UIViewContentMode.ScaleAspectFill
        
        profileImageView!.addSubview(profileImage)
        
       profileImageLabel.frame = CGRectMake((12/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(155/414)*UIScreen.mainScreen().bounds.size.width,(28/568)*UIScreen.mainScreen().bounds.size.height)
        
        //newEssayLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        
//        profileImageLabel.layer.cornerRadius = 8
//        profileImageLabel.layer.borderWidth = 4
//        profileImageLabel.layer.borderColor = UIColor.whiteColor().CGColor
        
        profileImageBtn.layer.cornerRadius = 4
        profileImageBtn.layer.borderWidth = 2
        profileImageBtn.layer.borderColor = UIColor.whiteColor().CGColor
        
        profileImageLabel.textColor = UIColor.blackColor()
        profileImageLabel.backgroundColor = UIColor(red: 26/255, green: 201/255, blue:112/255, alpha:1.0)
        profileImageLabel.textAlignment = NSTextAlignment.Center
        profileImageLabel.textColor = UIColor.whiteColor()
        profileImageLabel.text = "UPLOAD PHOTO"
        profileImageLabel.font = UIFont(name:"HelveticaNeue", size: 14.0)
        profileImageLabel.tag = 10
        
        profileImageBtn.addSubview(profileImageLabel)
        
        profileImageBtn.frame = CGRectMake((20/320)*UIScreen.mainScreen().bounds.size.width,(150/568)*UIScreen.mainScreen().bounds.size.height,(140/320)*UIScreen.mainScreen().bounds.size.width,(28/568)*UIScreen.mainScreen().bounds.size.height)
        
        profileImageBtn.backgroundColor = UIColor(red: 26/255, green: 201/255, blue:112/255, alpha:1.0)
        profileImageBtn.setTitle ("", forState: UIControlState.Normal)
        profileImageBtn.addTarget(self, action:"goToprofileImageBtn:",forControlEvents: UIControlEvents.TouchUpInside)

        // courseBtn.backgroundColor = UIColor.blackColor()
        
        profileImageView!.tag = 95
        
        profileImageView?.addSubview(profileImageBtn)
        
        profileImageBtn.tag = 10
        
   
        
        //////////////////////////////////////////////
    }
    
    func goToprofileImageBtn(sender: UIButton)
    {
        print("goToprofileImageBtn")
        
        courseLabel.textColor = UIColor.blackColor()
        messageLabel.textColor = UIColor.blackColor()
        profileLabel.textColor = UIColor.blackColor()
        activityLabel.textColor = UIColor.blackColor()
        dashBoardLAbel.textColor = UIColor.blackColor()
        requestLabel.textColor = UIColor.blackColor()
        accessCertificateLabel.textColor = UIColor.blackColor()
        editAccountsLabel.textColor = UIColor.blackColor()
        
        
        dashBoardView.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        courseView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        requestView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        accessCertificateView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        editAccountsView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        activityView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        messageView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        profileView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
    

    }

    func goTocourseBtn(sender: UIButton)
    {
        print("goTocourseBtn")
        courseLabel.textColor = UIColor.whiteColor()
    
        messageLabel.textColor = UIColor.blackColor()
        profileLabel.textColor = UIColor.blackColor()
        activityLabel.textColor = UIColor.blackColor()
//        dashBoardLAbel.textColor = UIColor.blackColor()
        requestLabel.textColor = UIColor.blackColor()
        accessCertificateLabel.textColor = UIColor.blackColor()
        editAccountsLabel.textColor = UIColor.blackColor()
        
//        dashBoardView.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        courseView!.backgroundColor = UIColor(patternImage: UIImage(named: "blue.png")!)
        requestView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        accessCertificateView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        editAccountsView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        activityView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        messageView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        profileView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
 
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SCMyCoursesz = storyboard.instantiateViewControllerWithIdentifier("SCMyCourses") as! SCMyCourses
        let navigationController = self.window?.rootViewController as! UINavigationController
        navigationController.pushViewController(SCMyCoursesz, animated: true)

    }
    func goTousernameBtn(sender: UIButton)
    {
        print("goTousernameBtn")
        
        courseLabel.textColor = UIColor.blackColor()
        messageLabel.textColor = UIColor.blackColor()
        profileLabel.textColor = UIColor.blackColor()
        activityLabel.textColor = UIColor.blackColor()
        dashBoardLAbel.textColor = UIColor.blackColor()
        requestLabel.textColor = UIColor.blackColor()
        accessCertificateLabel.textColor = UIColor.blackColor()
        editAccountsLabel.textColor = UIColor.blackColor()
        
        dashBoardView.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        courseView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        dashBoardView.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        requestView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        accessCertificateView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        editAccountsView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        activityView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        messageView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        profileView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        
        
    }
    
    func goTologoutBtn(sender: UIButton)
    {
        print("goTologoutBtn")
        courseLabel.textColor = UIColor.blackColor()
        messageLabel.textColor = UIColor.blackColor()
        profileLabel.textColor = UIColor.blackColor()
        activityLabel.textColor = UIColor.blackColor()
        dashBoardLAbel.textColor = UIColor.blackColor()
        requestLabel.textColor = UIColor.blackColor()
        accessCertificateLabel.textColor = UIColor.blackColor()
        editAccountsLabel.textColor = UIColor.blackColor()
        
        dashBoardView.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        courseView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        dashBoardView.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        requestView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        accessCertificateView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        editAccountsView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        activityView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        messageView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        profileView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
    }
    
    func goTomessageBtn(sender: UIButton)
    {
        print("goTomessageBtn")
        courseLabel.textColor = UIColor.blackColor()
        profileLabel.textColor = UIColor.blackColor()
        activityLabel.textColor = UIColor.blackColor()
        dashBoardLAbel.textColor = UIColor.blackColor()
        requestLabel.textColor = UIColor.blackColor()
        accessCertificateLabel.textColor = UIColor.blackColor()
        editAccountsLabel.textColor = UIColor.blackColor()
        
        messageLabel.textColor = UIColor.whiteColor()
        dashBoardView.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        courseView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        dashBoardView.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        requestView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        accessCertificateView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        editAccountsView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        activityView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        messageView!.backgroundColor = UIColor(patternImage: UIImage(named: "blue.png")!)
        profileView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SCSentz = storyboard.instantiateViewControllerWithIdentifier("SCSent") as! SCSent
        let navigationController = self.window?.rootViewController as! UINavigationController
        navigationController.pushViewController(SCSentz, animated: true)


    }
    
    func goToprofileBtn(sender: UIButton)
    {
        print("goToprofileBtn")
        profileLabel.textColor = UIColor.whiteColor()
        
        courseLabel.textColor = UIColor.blackColor()
        messageLabel.textColor = UIColor.blackColor()
        activityLabel.textColor = UIColor.blackColor()
        dashBoardLAbel.textColor = UIColor.blackColor()
        requestLabel.textColor = UIColor.blackColor()
        accessCertificateLabel.textColor = UIColor.blackColor()
        editAccountsLabel.textColor = UIColor.blackColor()
        
        dashBoardView.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        courseView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        dashBoardView.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        requestView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        accessCertificateView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        editAccountsView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        activityView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        messageView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        profileView!.backgroundColor = UIColor(patternImage: UIImage(named: "blue.png")!)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SCProfilez = storyboard.instantiateViewControllerWithIdentifier("SCProfile") as! SCProfile
        let navigationController = self.window?.rootViewController as! UINavigationController
        navigationController.pushViewController(SCProfilez, animated: true)


    }
    
    func goToactivityBtn(sender: UIButton)
    {
        print("goToactivityBtn")
        activityLabel.textColor = UIColor.whiteColor()
        
        courseLabel.textColor = UIColor.blackColor()
        messageLabel.textColor = UIColor.blackColor()
        profileLabel.textColor = UIColor.blackColor()
        dashBoardLAbel.textColor = UIColor.blackColor()
        requestLabel.textColor = UIColor.blackColor()
        accessCertificateLabel.textColor = UIColor.blackColor()
        editAccountsLabel.textColor = UIColor.blackColor()
        
        dashBoardView.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        courseView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        dashBoardView.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        requestView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        accessCertificateView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        editAccountsView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        activityView!.backgroundColor = UIColor(patternImage: UIImage(named: "blue.png")!)
        messageView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        profileView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SCPersonalz = storyboard.instantiateViewControllerWithIdentifier("SCPersonal") as! SCPersonal
        let navigationController = self.window?.rootViewController as! UINavigationController
        navigationController.pushViewController(SCPersonalz, animated: true)


    }
    
    func Home(sender: UIButton)
    {
        print("1st dashboard")
        dashBoardLAbel.textColor = UIColor.whiteColor()
        
        courseLabel.textColor = UIColor.blackColor()
        messageLabel.textColor = UIColor.blackColor()
        profileLabel.textColor = UIColor.blackColor()
        activityLabel.textColor = UIColor.blackColor()
        requestLabel.textColor = UIColor.blackColor()
        accessCertificateLabel.textColor = UIColor.blackColor()
        editAccountsLabel.textColor = UIColor.blackColor()
        
        dashBoardView.backgroundColor = UIColor(patternImage: UIImage(named: "blue.png")!)
        courseView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        requestView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        accessCertificateView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        editAccountsView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        activityView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        messageView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        profileView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SCDashboardz = storyboard.instantiateViewControllerWithIdentifier("SCDashboard") as! SCDashboard
        let navigationController = self.window?.rootViewController as! UINavigationController
        navigationController.pushViewController(SCDashboardz, animated: true)
    }
    func goTorequestBtn(sender: UIButton)
    {
        print("goTorequestBtn")
        requestLabel.textColor = UIColor.whiteColor()
        
        courseLabel.textColor = UIColor.blackColor()
        messageLabel.textColor = UIColor.blackColor()
        profileLabel.textColor = UIColor.blackColor()
        activityLabel.textColor = UIColor.blackColor()
        dashBoardLAbel.textColor = UIColor.blackColor()
        accessCertificateLabel.textColor = UIColor.blackColor()
        editAccountsLabel.textColor = UIColor.blackColor()
        
        dashBoardView.backgroundColor = UIColor(patternImage: UIImage(named:"greyPattern.png")!)
        courseView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        dashBoardView.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        requestView!.backgroundColor = UIColor(patternImage: UIImage(named: "blue.png")!)
        accessCertificateView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        editAccountsView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        activityView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        messageView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        profileView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SCRequestingCoursez = storyboard.instantiateViewControllerWithIdentifier("SCRequestingCourse") as! SCRequestingCourse
        let navigationController = self.window?.rootViewController as! UINavigationController
        navigationController.pushViewController(SCRequestingCoursez, animated: true)
    }
    func goToaccessCertificateBtn(sender: UIButton)
    {
        print("goToaccessCertificateBtn")
        accessCertificateLabel.textColor = UIColor.whiteColor()
        
        courseLabel.textColor = UIColor.blackColor()
        messageLabel.textColor = UIColor.blackColor()
        profileLabel.textColor = UIColor.blackColor()
        activityLabel.textColor = UIColor.blackColor()
        dashBoardLAbel.textColor = UIColor.blackColor()
        requestLabel.textColor = UIColor.blackColor()
        editAccountsLabel.textColor = UIColor.blackColor()
        
        dashBoardView.backgroundColor = UIColor(patternImage: UIImage(named:"greyPattern.png")!)
        courseView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        dashBoardView.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        requestView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        accessCertificateView!.backgroundColor = UIColor(patternImage: UIImage(named: "blue.png")!)
        editAccountsView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        activityView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        messageView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        profileView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SCRequestingCoursez = storyboard.instantiateViewControllerWithIdentifier("SCRequestingCourse") as! SCRequestingCourse
        let navigationController = self.window?.rootViewController as! UINavigationController
        navigationController.pushViewController(SCRequestingCoursez, animated: true)

    }
    func goToeditAccountsBtn(sender: UIButton)
    {
        print("goToaccessCertificateBtn")
        editAccountsLabel.textColor = UIColor.whiteColor()
        
        courseLabel.textColor = UIColor.blackColor()
        messageLabel.textColor = UIColor.blackColor()
        profileLabel.textColor = UIColor.blackColor()
        activityLabel.textColor = UIColor.blackColor()
        dashBoardLAbel.textColor = UIColor.blackColor()
        requestLabel.textColor = UIColor.blackColor()
        accessCertificateLabel.textColor = UIColor.blackColor()
        
        dashBoardView.backgroundColor = UIColor(patternImage: UIImage(named:"greyPattern.png")!)
        courseView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        dashBoardView.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        requestView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        accessCertificateView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        editAccountsView!.backgroundColor = UIColor(patternImage: UIImage(named: "blue.png")!)
        activityView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        messageView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        profileView!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SCEditAccountsz = storyboard.instantiateViewControllerWithIdentifier("SCEditAccounts") as! SCEditAccounts
        let navigationController = self.window?.rootViewController as! UINavigationController
        navigationController.pushViewController(SCEditAccountsz, animated: true)

    }
    

    func closeMenuBtnClicked(sender: UIButton)
    {
        // print("menu btn pressed")
        sender.hidden = true
        
        print("-myMenuView.frame.size.width and x in appdelegate \(-myMenuView.frame.size.width) \(x)")
        x = -myMenuView.frame.size.width
        
        UIView.animateWithDuration(0.5, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = -myMenuView.frame.size.width
                
        })
        
    }


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool
    {
             creatingMenu()
        
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

