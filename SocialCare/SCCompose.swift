
//
//  SCCompose.swift
//  SocialCare
//
//  Created by Mrinal Khullar on 5/2/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class SCCompose: UIViewController,UITextFieldDelegate
{
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0
   
    @IBOutlet weak var enterMessagelabel: UILabel!
    @IBOutlet weak var sentCover: UIView!
    @IBOutlet weak var sentBtnOutlet: UIButton!
   
    @IBOutlet weak var messagetextView: UITextView!
    @IBOutlet weak var subjectTextField: UITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        sentCover.layer.cornerRadius = 8
        sentCover.layer.borderWidth = 3
        sentCover.layer.borderColor = UIColor.clearColor().CGColor
        
        sentBtnOutlet.layer.cornerRadius = 8
        sentBtnOutlet.layer.borderWidth = 3
        sentBtnOutlet.layer.borderColor = UIColor.clearColor().CGColor
        
        messagetextView.layer.cornerRadius = 8
        messagetextView.layer.borderWidth = 3
        messagetextView.layer.borderColor = UIColor.clearColor().CGColor
        
        subjectTextField.layer.cornerRadius = 8
        subjectTextField.layer.borderWidth = 3
        subjectTextField.layer.borderColor = UIColor.clearColor().CGColor

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool)
    {
         closeMenuBtn.hidden = true
        
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func menuBtn(sender: AnyObject)
    {
        myMenuView.hidden = false
        
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
        else
        {
            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.5, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })
    }
    
    @IBAction func backBtn(sender: AnyObject) {
    }
    @IBAction func sentBtn(sender: AnyObject) {
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        subjectTextField.resignFirstResponder()

        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        subjectTextField.resignFirstResponder()
    }
    
    

}
