//
//  SCLogin.swift
//  SocialCare
//
//  Created by Mrinal Khullar on 4/25/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class SCLogin: UIViewController
{

    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var rememberImageView: UIImageView!
    @IBOutlet weak var loginOutlet: UIButton!
    @IBOutlet weak var rememberMeOutlet: UIButton!
    @IBOutlet weak var passwordTxtFiled: UITextField!
    @IBOutlet weak var usernameTxtField: UITextField!
    
    //+++++++++++++++++++++++++++++//
    var isChecked:Bool = Bool()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        placeHolderMethodForSCLogin()
        
        self.navigationController?.navigationBar.hidden = true
        
        loginOutlet.layer.cornerRadius = 6
        loginOutlet.layer.borderWidth = 2
        loginOutlet.layer.borderColor = UIColor.clearColor().CGColor
        
        loginView.layer.cornerRadius = 8
        loginView.layer.borderWidth = 2
        loginView.layer.borderColor = UIColor.clearColor().CGColor
        
        usernameTxtField.layer.cornerRadius = 4
        usernameTxtField.layer.borderWidth = 1.5
        usernameTxtField.layer.borderColor = UIColor.clearColor().CGColor
        
        
        passwordTxtFiled.layer.cornerRadius = 4
        passwordTxtFiled.layer.borderWidth = 1.5
        passwordTxtFiled.layer.borderColor = UIColor.clearColor().CGColor
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func placeHolderMethodForSCLogin()
    {
        let paddingView = UIView(frame: CGRectMake(0, 0, 15, usernameTxtField.frame.height))
        usernameTxtField.leftView = paddingView
        usernameTxtField.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewPwd = UIView(frame: CGRectMake(0, 0, 15, passwordTxtFiled.frame.height))
        passwordTxtFiled.leftView = paddingViewPwd
        passwordTxtFiled.leftViewMode = UITextFieldViewMode.Always

        
        usernameTxtField.attributedPlaceholder = NSAttributedString(string:"USERNAME", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
          passwordTxtFiled.attributedPlaceholder = NSAttributedString(string:"PASSWORD", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
    }
    

    @IBAction func loginBtn(sender: AnyObject)
    {
        myMenuView.hidden = true
        let SCDashboardz = self.storyboard!.instantiateViewControllerWithIdentifier("SCDashboard") as! SCDashboard
        
        self.navigationController?.pushViewController(SCDashboardz, animated: true)
    }
    @IBAction func rememberMeBtn(sender: AnyObject)
    {
        if (isChecked == false)
        {
            rememberImageView.image = UIImage(named:"check.png")
            isChecked = true
        }
        else
        {
            rememberImageView.image = UIImage(named:"uncheck.png")
            isChecked = false
        }
    }

    @IBAction func forgotpasswordBtn(sender: AnyObject) {
    }

}
