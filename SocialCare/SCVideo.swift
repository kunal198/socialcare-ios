
//
//  SCVideo.swift
//  SocialCare
//
//  Created by Mrinal Khullar on 4/26/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit
import MediaPlayer

class SCVideo: UIViewController
{
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0

    var moviePlayer : MPMoviePlayerController?
    var _isFullScreen:Bool = Bool()
    @IBOutlet weak var lowerWhiteView: UIView!
    @IBOutlet weak var centerWhiteView: UIView!
    @IBOutlet weak var upperWhiteView: UIView!
    @IBOutlet weak var nextBtnOutlet: UIButton!
    @IBOutlet weak var nextBtnView: UIView!
    @IBOutlet weak var lowerView: UIView!
    @IBOutlet weak var redTextView: UITextView!
    @IBOutlet weak var audioView: UIView!
    @IBOutlet weak var upperview: UIView!
    
    @IBOutlet weak var blackView: UIView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
    
        blackView.layer.cornerRadius = 8
        blackView.layer.borderWidth = 4
        blackView.layer.borderColor = UIColor.clearColor().CGColor

        
        nextBtnView.layer.cornerRadius = 8
        nextBtnView.layer.borderWidth = 4
        nextBtnView.layer.borderColor = UIColor.clearColor().CGColor

        
        nextBtnOutlet.layer.cornerRadius = 6
        nextBtnOutlet.layer.borderWidth = 2
        nextBtnOutlet.layer.borderColor = UIColor.clearColor().CGColor
        
        redTextView.layer.cornerRadius = 3
        redTextView.layer.borderWidth = 1.5
        redTextView.layer.borderColor = UIColor.whiteColor().CGColor
        
        upperview.layer.cornerRadius = 6
        upperview.layer.borderWidth = 2
        upperview.layer.borderColor = UIColor.clearColor().CGColor
        
        upperWhiteView.layer.cornerRadius = 6
        upperWhiteView.layer.borderWidth = 2
        upperWhiteView.layer.borderColor = UIColor.clearColor().CGColor
        
        
        centerWhiteView.layer.cornerRadius = 6
        centerWhiteView.layer.borderWidth = 2
        centerWhiteView.layer.borderColor = UIColor.clearColor().CGColor
        
        lowerWhiteView.layer.cornerRadius = 6
        lowerWhiteView.layer.borderWidth = 2
        lowerWhiteView.layer.borderColor = UIColor.clearColor().CGColor


        // Do any additional setup after loading the view.
    }
    
    func playVideo()
    {
        let path = NSBundle.mainBundle().pathForResource("Jennifer Lopez Same Girl Full HD Video Song(mirchifun.in)", ofType:"mp4")
        let url = NSURL.fileURLWithPath(path!)
        moviePlayer = MPMoviePlayerController(contentURL: url)
        if let player = moviePlayer
        {
            player.view.frame = self.view.bounds
            player.controlStyle = MPMovieControlStyle.Embedded
            player.prepareToPlay()
            player.scalingMode = .AspectFill
            self.view.addSubview(player.view)
        }

        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "videoMPMoviePlayerPlaybackDidFinishNotification:", name: MPMoviePlayerPlaybackDidFinishNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "videoMPMoviePlayerWillExitFullscreenNotification:", name: MPMoviePlayerWillExitFullscreenNotification, object: nil);
        
          _isFullScreen = true
    }
    
    
    func videoMPMoviePlayerWillExitFullscreenNotification(notification:NSNotification)
    {
        
      
        UIView.animateWithDuration(0.7, animations:
            {
                
                self.moviePlayer?.view.removeFromSuperview()
                self.moviePlayer?.stop()
        })
        
//        let value = UIInterfaceOrientation.LandscapeLeft.rawValue ;// UIInterfaceOrientation.LandscapeRight.rawValue
//        UIDevice.currentDevice().setValue(value, forKey: "orientation")
     }
    
    func videoMPMoviePlayerPlaybackDidFinishNotification(notification:NSNotification)
    {
        UIView.animateWithDuration(0.7, animations:
            {

              self.moviePlayer?.view.removeFromSuperview()
              self.moviePlayer?.stop()
        })
        
//        let value = UIInterfaceOrientation.LandscapeLeft.rawValue ;// UIInterfaceOrientation.LandscapeRight.rawValue
//        UIDevice.currentDevice().setValue(value, forKey: "orientation")
    }
    
    override func viewWillAppear(animated: Bool)
    {
         closeMenuBtn.hidden = true
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func forwardBtn(sender: AnyObject)
    {
        isUnitStarted = true
        isUnitOne = true
        let SCStartCoursez = self.storyboard!.instantiateViewControllerWithIdentifier("SCStartCourse") as! SCStartCourse
        
        self.navigationController?.pushViewController(SCStartCoursez, animated: true)
    }
    @IBAction func menuBtn(sender: AnyObject)
    {
        myMenuView.hidden = false
        
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
        else
        {
            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.5, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })
    }
    @IBAction func playBtn(sender: AnyObject)
    {
        playVideo()
    }
    
//    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask
//    {
//        return UIInterfaceOrientationMask.LandscapeLeft
//    }

    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
   
    @IBAction func nextUnitBtn(sender: AnyObject)
    {
          isUnitStarted = true
        isUnitOne = true
        let SCStartCoursez = self.storyboard!.instantiateViewControllerWithIdentifier("SCStartCourse") as! SCStartCourse
        
        self.navigationController?.pushViewController(SCStartCoursez, animated: true)
    }
}
