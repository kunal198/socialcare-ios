//
//  SCSent.swift
//  SocialCare
//
//  Created by Mrinal Khullar on 5/2/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class SCSent: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate
{
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0
    
    var sentArray:NSMutableArray = NSMutableArray()
    var InboxArray:NSMutableArray = NSMutableArray()
    var isInboxSelected:Bool = Bool()
    var isComposeSelected:Bool = Bool()
    
    //////////////////////////////////
    @IBOutlet weak var sentTableView: UITableView!
    @IBOutlet weak var subjectTextField: UITextField!
    
    @IBOutlet weak var composeOutlet: UIButton!
    @IBOutlet weak var sentOutlet: UIButton!
    @IBOutlet weak var inboxOutlet: UIButton!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var userOrfriendLabel: UILabel!
    @IBOutlet weak var messageTextFiled: UITextField!
    @IBOutlet weak var sentBtnOutlet: UIButton!
   
    @IBOutlet weak var sentView: UIView!
    @IBOutlet weak var unreadOutlet: UIButton!
    @IBOutlet weak var readOutlet: UIButton!
    @IBOutlet weak var selectTxtField: UITextField!
    @IBOutlet weak var deleteBtnOutlet: UIButton!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        sentView.layer.cornerRadius = 8
        sentView.layer.borderWidth = 3
        sentView.layer.borderColor = UIColor.clearColor().CGColor
        
        sentBtnOutlet.layer.cornerRadius = 8
        sentBtnOutlet.layer.borderWidth = 3
        sentBtnOutlet.layer.borderColor = UIColor.clearColor().CGColor
        
        messageTextFiled.layer.cornerRadius = 8
        messageTextFiled.layer.borderWidth = 3
        messageTextFiled.layer.borderColor = UIColor.clearColor().CGColor
        
        subjectTextField.layer.cornerRadius = 8
        subjectTextField.layer.borderWidth = 3
        subjectTextField.layer.borderColor = UIColor.clearColor().CGColor
        
        let paddingViewUserName = UIView(frame: CGRectMake(0, 0, 15, subjectTextField.frame.height))
        subjectTextField.leftView = paddingViewUserName
        subjectTextField.leftViewMode = UITextFieldViewMode.Always
        
        
        subjectTextField.attributedPlaceholder = NSAttributedString(string:"SUBJECT", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
        
        let paddingViewMessage = UIView(frame: CGRectMake(0, 0, 15, messageTextFiled.frame.height))
        messageTextFiled.leftView = paddingViewMessage
        messageTextFiled.leftViewMode = UITextFieldViewMode.Always
        
//        messagetextView 
//        messagetextView.attributedPlaceholder = NSAttributedString(string:"MESSAGE", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
        
        fillSentLisitng()
        fillInboxLisitng()
        
        sentTableView.backgroundColor = UIColor.clearColor()

            unreadOutlet.hidden = true
            readOutlet.hidden = true
        
        isInboxSelected = true

        // Do any additional setup after loading the view.
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        self.view.frame.origin.y = 0.0
    }
    
    func textFieldDidBeginEditing(textField: UITextField)
    {
//        if textField == passwordTextFieldRegistration
//        {
//            self.view.frame.origin.y = -40.0
//        }
//        else if textField == rePasswordTextFieldRegistration
//        {
//            self.view.frame.origin.y = -55.0
//        }
    }
    
    override func viewWillAppear(animated: Bool)
    {
         closeMenuBtn.hidden = true
        
        if( isInboxSelected == true)
        {
            isComposeSelected = false
            unreadOutlet.hidden = false
            readOutlet.hidden = false
            
            selectTxtField.hidden = false
            deleteBtnOutlet.hidden = false
            
            sentTableView.hidden = false
        }
        else
        {
            isComposeSelected = false
            unreadOutlet.hidden = true
            readOutlet.hidden = true
            
            userOrfriendLabel.hidden = false
            selectTxtField.hidden = false
            
            sentTableView.hidden = false
        }
        
        if(isComposeSelected == true)
        {
            isInboxSelected = false
            readOutlet.hidden = true
            readOutlet.hidden = false
            
            sentView.hidden = false
            messageTextFiled.hidden = false
            subjectTextField.hidden = false
            usernameLabel.hidden = false
            userOrfriendLabel.hidden = true
            selectTxtField.hidden = true
            
            sentTableView.hidden = true
        }

            self.view.addSubview(closeMenuBtn)
            self.view.addSubview(myMenuView)
            myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
            
            x = -myMenuView.frame.size.width
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fillSentLisitng()
    {
        selectTxtField.layer.cornerRadius = 8
        selectTxtField.layer.borderWidth = 3
        selectTxtField.layer.borderColor = UIColor.clearColor().CGColor
        
        deleteBtnOutlet.layer.cornerRadius = 8
        deleteBtnOutlet.layer.borderWidth = 3
        deleteBtnOutlet.layer.borderColor = UIColor.clearColor().CGColor
        
        readOutlet.layer.cornerRadius = 8
        readOutlet.layer.borderWidth = 3
        readOutlet.layer.borderColor = UIColor.clearColor().CGColor
        
        unreadOutlet.layer.cornerRadius = 8
        unreadOutlet.layer.borderWidth = 3
        unreadOutlet.layer.borderColor = UIColor.clearColor().CGColor
        
        sentArray .addObject(["sentName":"Demo Text","sentDesc":"Lorem ipsum dolor sit er elit lamet, consectetaur ","sentFrom":"Jay Yassir","sentDate":"15/04/15","sentTime":"at 9:30 am","sentImage":"image3.jpg"])
        sentArray .addObject(["sentName":"Demo Text","sentDesc":"Lorem ipsum dolor sit er elit lamet, consectetaur ","sentFrom":"Jay Yassir","sentDate":"15/04/15","sentTime":"at 9:30 am","sentImage":"image3.jpg"])
        sentArray .addObject(["sentName":"Demo Text","sentDesc":"Lorem ipsum dolor sit er elit lamet, consectetaur ","sentFrom":"Jay Yassir","sentDate":"15/04/15","sentTime":"at 9:30 am","sentImage":"image3.jpg"])
        sentArray .addObject(["sentName":"Demo Text","sentDesc":"Lorem ipsum dolor sit er elit lamet, consectetaur ","sentFrom":"Jay Yassir","sentDate":"15/04/15","sentTime":"at 9:30 am","sentImage":"image3.jpg"])
        sentArray .addObject(["sentName":"Demo Text","sentDesc":"Lorem ipsum dolor sit er elit lamet, consectetaur ","sentFrom":"Jay Yassir","sentDate":"15/04/15","sentTime":"at 9:30 am","sentImage":"image3.jpg"])
        
        // print("locationListingArray is\(locationListingArray)")
    }
    
    func fillInboxLisitng()
    {
        InboxArray .addObject(["sentName":"Course result available","sentDesc":"You have obtained 20/100 in course: Appropriate Adults[....]","sentFrom":"Jay Yassir","sentDate":"15/04/15","sentTime":"at 9:30 am","sentImage":"image4.jpeg"])
        InboxArray .addObject(["sentName":"Course result available","sentDesc":"You have obtained 20/100 in course: Appropriate Adults[....]","sentFrom":"Jay Yassir","sentDate":"15/04/15","sentTime":"at 9:30 am","sentImage":"image4.jpeg"])
        InboxArray .addObject(["sentName":"Course result available","sentDesc":"You have obtained 20/100 in course: Appropriate Adults[....]","sentFrom":"Jay Yassir","sentDate":"15/04/15","sentTime":"at 9:30 am","sentImage":"image4.jpeg"])
        InboxArray .addObject(["sentName":"Course result available","sentDesc":"You have obtained 20/100 in course: Appropriate Adults[....]","sentFrom":"Jay Yassir","sentDate":"23/12/2015","sentTime":"at 1:22 pm am","sentImage":"image4.jpeg"])
        InboxArray .addObject(["sentName":"Course result available","sentDesc":"You have obtained 20/100 in course: Appropriate Adults[....]","sentFrom":"Jay Yassir","sentDate":"23/12/2015","sentTime":"at 1:22 pm am","sentImage":"image4.jpeg"])
        
        // print("locationListingArray is\(locationListingArray)")
    }
    
    @IBAction func deleteBtn(sender: AnyObject)
    {
        
    }
    @IBAction func composeBtn(sender: AnyObject)
    {
        isInboxSelected = false
        readOutlet.hidden = true
        
        sentView.hidden = false
        messageTextFiled.hidden = false
        subjectTextField.hidden = false
        usernameLabel.hidden = false
        userOrfriendLabel.hidden = false
        deleteBtnOutlet.hidden = true
        selectTxtField.hidden = true
        unreadOutlet.hidden = true
        
        isComposeSelected = true
        
        sentTableView.hidden = true
        
        composeOutlet.backgroundColor = UIColor(red: 252.0/255.0, green: 61.0/255.0, blue: 61.0/255.0, alpha: 1.0)
        sentOutlet.backgroundColor = UIColor(red: 231.0/255.0, green: 231.0/255.0, blue: 231.0/255.0, alpha: 1.0)
        inboxOutlet.backgroundColor = UIColor(red: 231.0/255.0, green: 231.0/255.0, blue: 231.0/255.0, alpha: 1.0)
        
        composeOutlet.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        sentOutlet.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        inboxOutlet.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        
    }
    @IBAction func sentBtn(sender: AnyObject)
    {
        isComposeSelected = false
        
        unreadOutlet.hidden = true
        readOutlet.hidden = true
        isInboxSelected = false
        sentTableView.hidden = false
        sentTableView.reloadData()
        
        sentView.hidden = true
        messageTextFiled.hidden = true
        subjectTextField.hidden = true
        usernameLabel.hidden = true
        userOrfriendLabel.hidden = true
        
        deleteBtnOutlet.hidden = false
        selectTxtField.hidden = false
        
        sentOutlet.backgroundColor = UIColor(red: 252.0/255.0, green: 61.0/255.0, blue: 61.0/255.0, alpha: 1.0)
        composeOutlet.backgroundColor = UIColor(red: 231.0/255.0, green: 231.0/255.0, blue: 231.0/255.0, alpha: 1.0)
        inboxOutlet.backgroundColor = UIColor(red: 231.0/255.0, green: 231.0/255.0, blue: 231.0/255.0, alpha: 1.0)
        
         sentOutlet.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        composeOutlet.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        inboxOutlet.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
    }

    @IBAction func sentMessageBtn(sender: AnyObject)
    {
        
    }
    
    @IBAction func inboxBtn(sender: AnyObject)
    {
        isComposeSelected = false
        
        isInboxSelected = true
        unreadOutlet.hidden = false
        readOutlet.hidden = false
        sentTableView.hidden = false
        sentTableView.reloadData()
        
        sentView.hidden = true
        messageTextFiled.hidden = true
        subjectTextField.hidden = true
        usernameLabel.hidden = true
        userOrfriendLabel.hidden = true
        
        deleteBtnOutlet.hidden = false
        selectTxtField.hidden = false
        
        inboxOutlet.backgroundColor = UIColor(red: 252.0/255.0, green: 61.0/255.0, blue: 61.0/255.0, alpha: 1.0)
        sentOutlet.backgroundColor = UIColor(red: 231.0/255.0, green: 231.0/255.0, blue: 231.0/255.0, alpha: 1.0)
        composeOutlet.backgroundColor = UIColor(red: 231.0/255.0, green: 231.0/255.0, blue: 231.0/255.0, alpha: 1.0)
        
        inboxOutlet.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        sentOutlet.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        composeOutlet.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        

    }
    @IBAction func menuBtn(sender: AnyObject)
    {
        myMenuView.hidden = false
        
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
        else
        {
            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.5, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })
    }
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 125.0
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1;
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(isInboxSelected == true)
        {
            return InboxArray.count
        }
        else
        {
            return sentArray.count
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("SCCustomCell", forIndexPath: indexPath) as! SCCustomCell
        
        cell.sentCoverView.layer.cornerRadius = 8
        cell.sentCoverView.layer.borderWidth = 3
        cell.sentCoverView.layer.borderColor = UIColor(red: 70.0/255.0, green: 139.0/255.0, blue: 185.0/255.0, alpha: 1.0).CGColor
        
        cell.whiteSelectingView.layer.cornerRadius = 8
        cell.whiteSelectingView.layer.borderWidth = 3
        cell.whiteSelectingView.layer.borderColor = UIColor.clearColor().CGColor
        
        cell.sentImageView.layer.borderWidth = 4
        cell.sentImageView.layer.cornerRadius =  cell.sentImageView.frame.size.width/2
        cell.sentImageView.layer.borderColor = UIColor.init(white: 0.5, alpha: 1.0).CGColor
        cell.sentImageView.clipsToBounds = true
        cell.sentImageView!.contentMode = UIViewContentMode.ScaleAspectFill
        
        if(isInboxSelected == true)
        {
            cell.sentTitle.text = InboxArray[indexPath.row].valueForKey("sentName") as? String
            let courseImages = InboxArray[indexPath.row].valueForKey("sentImage") as? String
            cell.sentImageView.image = UIImage(named:courseImages!)
            cell.sentDesc.text = InboxArray[indexPath.row].valueForKey("sentDesc") as? String
            cell.sentDesc.numberOfLines = 4
            cell.sentDate.text = InboxArray[indexPath.row].valueForKey("sentDate") as? String
            cell.sentDay.text = InboxArray[indexPath.row].valueForKey("sentTime") as? String
        }
        else
        {
            cell.sentTitle.text = sentArray[indexPath.row].valueForKey("sentName") as? String
            let courseImages = sentArray[indexPath.row].valueForKey("sentImage") as? String
            cell.sentImageView.image = UIImage(named:courseImages!)
            cell.sentDesc.text = sentArray[indexPath.row].valueForKey("sentDesc") as? String
            cell.sentDesc.numberOfLines = 4
            cell.sentDate.text = sentArray[indexPath.row].valueForKey("sentDate") as? String
            cell.sentDay.text = sentArray[indexPath.row].valueForKey("sentTime") as? String
        }

        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
        // Cell.separatorInset = UIEdgeInsetsZero;
        tableView.separatorInset = UIEdgeInsetsZero;
        tableView.separatorColor = UIColor.clearColor()
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        
    }

}
