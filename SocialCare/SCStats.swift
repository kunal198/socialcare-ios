//
//  SCStats.swift
//  SocialCare
//
//  Created by Mrinal Khullar on 4/28/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class SCStats: UIViewController,UIScrollViewDelegate
{
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0

    @IBOutlet weak var statsScrollView: UIScrollView!
    
    @IBOutlet weak var statsOutlet: UIButton!
    @IBOutlet weak var resultsOutlet: UIButton!
    @IBOutlet weak var coursesOutlet: UIButton!
    ////////////////////////////////////////////////////
    
    var dataObj:NSMutableArray = NSMutableArray()
    var noOfItems = 0
    var index = 0
    var mainViewWrapperY = CGFloat()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        fillSTATSArray()
        addContent()
        
        statsOutlet!.backgroundColor = UIColor(patternImage: UIImage(named: "red.png")!)
        resultsOutlet!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        coursesOutlet!.backgroundColor = UIColor(patternImage: UIImage(named: "greyPattern.png")!)
        

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool)
    {
         closeMenuBtn.hidden = true
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func resultBtn(sender: AnyObject)
    {
        
    }
    @IBAction func myCourseBtn(sender: AnyObject)
    {
        let SCMyCoursesz = self.storyboard!.instantiateViewControllerWithIdentifier("SCMyCourses") as! SCMyCourses
        
        self.navigationController?.pushViewController(SCMyCoursesz, animated: false)
    }
    @IBAction func menuBtn(sender: AnyObject)
    {
        myMenuView.hidden = false
        
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
        else
        {
            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.5, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })
    }
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(false)
    }
    
    func fillSTATSArray()
    {
        dataObj .addObject(["courseTitle":"ADOPTION FOSTERING","CourseImage":"image3.jpg","courseAvgPercentage":"Average Percentage:N/A","DownloadImage":"downloadStat.png","centerImage":"center.png","starImage":"star.png","score":"My SCORE : 100/100","plusImage":"plusImage.png"])
        dataObj .addObject(["courseTitle":"ADOPTION FOSTERING","CourseImage":"image4.jpeg","courseAvgPercentage":"Average Percentage:N/A","DownloadImage":"downloadStat.png","centerImage":"center.png","starImage":"star.png","score":"UNDER EVALUTION","plusImage":"plusImage.png"])
        dataObj .addObject(["courseTitle":"ADOPTION FOSTERING","CourseImage":"image3.jpg","courseAvgPercentage":"Average Percentage:N/A","DownloadImage":"downloadStat.png","centerImage":"center.png","starImage":"star.png","score":"45/100","plusImage":"plusImage.png"])
        dataObj .addObject(["courseTitle":"ADOPTION FOSTERING","CourseImage":"image4.jpeg","courseAvgPercentage":"Average Percentage:N/A","DownloadImage":"downloadStat.png","centerImage":"center.png","starImage":"star.png","score":"My SCORE : 65/100","plusImage":"plusImage.png"])
    }
    
    func addContent()
    {
        noOfItems = dataObj.count
        print("noOfItems is\(CGFloat(noOfItems))")
        
        let height = (statsScrollView.frame.size.height * CGFloat(noOfItems-1)) - 105
        print("height is \(height)")
        statsScrollView.contentSize = CGSizeMake(statsScrollView.frame.size.width, height)

        for( var i = 0; i < noOfItems; i++)
        {
 
            let mainViewWrapper = UIView()
           
//            print("mainViewWrapperY is \(mainViewWrapperY)")
  
            mainViewWrapper.frame = CGRectMake(0 ,mainViewWrapperY, statsScrollView.frame.size.width,statsScrollView.frame.size.height/1.5)
            mainViewWrapper.backgroundColor = UIColor.clearColor()
            statsScrollView.addSubview(mainViewWrapper)
//            print("mainViewWrapper is \(mainViewWrapper)")
            
           let screenSize = UIScreen.mainScreen().bounds.size
            
            if(screenSize.height == 568.0)
            {
                mainViewWrapperY = mainViewWrapperY + 240.0
            }
            else if(screenSize.height == 667.0)
            {
               mainViewWrapperY = mainViewWrapperY + 280.0
            }
            else if(screenSize.height == 736.0)
            {
                 mainViewWrapperY = mainViewWrapperY + 320.0
            }
            else if(screenSize.height == 480.0)
            {
                mainViewWrapperY = mainViewWrapperY + 200.0
            }
            else
            {
                print("not iphone device....")
            }
            
            ////////////////////////
            
            let widthOfFirstView = statsScrollView.frame.size.width/2-12.5
            let widthOfSecondView = widthOfFirstView - 6.2
            
            let xOfSecondView = widthOfFirstView + 20
            let heightOfView = mainViewWrapper.frame.size.height - 40
            
            let FirstView = UIView()
            FirstView.frame = CGRectMake(10 ,10,widthOfFirstView,heightOfView)
            FirstView.backgroundColor = UIColor(red:252.0/255.0, green:61.0/255.0, blue:61.0/255.0, alpha:1.0)
            mainViewWrapper.addSubview(FirstView)
            
            FirstView.layer.cornerRadius = 8
            FirstView.layer.borderWidth = 3
            FirstView.layer.borderColor = UIColor(red:232.0/255.0, green:188.0/255.0, blue:44.0/255.0, alpha:1.0).CGColor
            
            let SecondView = UIView()
            SecondView.frame = CGRectMake(xOfSecondView ,10,widthOfSecondView,heightOfView)
            SecondView.backgroundColor = UIColor(red:252.0/255.0, green:61.0/255.0, blue:61.0/255.0, alpha:1.0)
            mainViewWrapper.addSubview(SecondView)
            
            SecondView.layer.cornerRadius = 8
            SecondView.layer.borderWidth = 3
            SecondView.layer.borderColor = UIColor(red:232.0/255.0, green:188.0/255.0, blue:44.0/255.0, alpha:1.0).CGColor
            
            ///////////////////////////////////
            
            let xOfFirstImageView = (FirstView.frame.size.width/2.1)/2
            let widthOfFirstImageView = (FirstView.frame.size.width/1.9)
            
//            let FirstimageView:UIImageView = UIImageView()CourseImage
            let FirstimageName = dataObj.objectAtIndex(i).valueForKey("CourseImage") as? String
            let Firstimage = UIImage(named:FirstimageName!)
            
            let FirstimageView = UIImageView(image: Firstimage!)
            FirstimageView.frame = CGRectMake(xOfFirstImageView ,15,widthOfFirstImageView,widthOfFirstImageView)
            FirstimageView.backgroundColor = UIColor.blackColor()
            FirstView.addSubview(FirstimageView)
//            print("widthOfFirstImageView is \(widthOfFirstImageView)")
            
            FirstimageView.layer.cornerRadius = FirstimageView.frame.size.height/2
            FirstimageView.layer.borderWidth = 4
            FirstimageView.layer.borderColor = UIColor.clearColor().CGColor
            FirstimageView.layer.masksToBounds = true
            FirstimageView.clipsToBounds = true
            FirstimageView.contentMode = UIViewContentMode.ScaleAspectFill
            
            let xOfSecondImageView = (SecondView.frame.size.width/2.1)/2
            let widthOfSecondImageView = (SecondView.frame.size.width/1.9)
   
//            let SecondimageView:UIImageView = UIImageView()
            let SecondimageName = dataObj.objectAtIndex(i).valueForKey("CourseImage") as? String
            let Secondimage = UIImage(named:SecondimageName!)
            let SecondimageView = UIImageView(image: Firstimage!)
            SecondimageView.frame = CGRectMake(xOfSecondImageView ,15,widthOfSecondImageView,widthOfSecondImageView)
            SecondimageView.backgroundColor = UIColor.yellowColor()
            SecondView.addSubview(SecondimageView)
//            print("widthOfSecondImageView is \(widthOfSecondImageView)")
            
            SecondimageView.layer.cornerRadius = SecondimageView.frame.size.height/2
            SecondimageView.layer.borderWidth = 4
            SecondimageView.layer.borderColor =  UIColor.clearColor().CGColor
            SecondimageView.layer.masksToBounds = true
            SecondimageView.clipsToBounds = true
            SecondimageView.contentMode = UIViewContentMode.ScaleAspectFill
            
            ///////////////////////////////////////
            
            let yOfFirstName = FirstimageView.frame.size.height + 20
            let widthOfFirstName = FirstView.frame.size.width
            let xOfFirstName = (FirstimageView.frame.size.width/2.3)/2
            
            var FirstName = UILabel(frame: CGRectMake(0,yOfFirstName,widthOfFirstName, 30))
            FirstName.textAlignment = NSTextAlignment.Center
            FirstName.text = dataObj.objectAtIndex(i).valueForKey("courseTitle") as? String
            FirstName.font = UIFont (name: "HelveticaNeue-Bold", size: 12)
            FirstName.textColor = UIColor.whiteColor()
            FirstView.addSubview(FirstName)
            
            let yOfSecondName = SecondimageView.frame.size.height + 20
            let widthOfSecondName = SecondView.frame.size.width
            let xOfSecondName = (SecondimageView.frame.size.width/2.3)/2
            
            var SecondName = UILabel(frame: CGRectMake(0,yOfSecondName,widthOfSecondName, 30))
            SecondName.textAlignment = NSTextAlignment.Center
            SecondName.text = dataObj.objectAtIndex(i).valueForKey("courseTitle") as? String
            SecondName.font = UIFont (name: "HelveticaNeue-Bold", size: 12)
            SecondName.textColor = UIColor.whiteColor()
            SecondView.addSubview(SecondName)
            
            ////////////////////////////////////////
            
            let yOfFirstAvgPercentageLabel = FirstName.frame.origin.y + 23
//            print("yOfFirstAvgPercentageLabel is \(yOfFirstAvgPercentageLabel)")
            let widthOfFirstAvgPercentageLabel = FirstName.frame.size.width
            
            var FirstAvgPercentageLabel = UILabel(frame: CGRectMake(0,yOfFirstAvgPercentageLabel,widthOfFirstAvgPercentageLabel, 15))
            FirstAvgPercentageLabel.textAlignment = NSTextAlignment.Center
            FirstAvgPercentageLabel.text = "Average Percentage : N/A"
            FirstAvgPercentageLabel.font = UIFont (name: "Helvetica Neue", size: 10)
            FirstAvgPercentageLabel.textColor = UIColor.whiteColor()
            FirstView.addSubview(FirstAvgPercentageLabel)
            
            let yOfSecondAvgPercentageLabel = SecondName.frame.origin.y + 23
//            print("yOfFirstAvgPercentageLabel is \(yOfSecondAvgPercentageLabel)")
            let widthOfSecondAvgPercentageLabel = SecondName.frame.size.width
            
            var SecondAvgPercentageLabel = UILabel(frame: CGRectMake(0,yOfSecondAvgPercentageLabel,widthOfSecondAvgPercentageLabel, 17))
            SecondAvgPercentageLabel.textAlignment = NSTextAlignment.Center
            SecondAvgPercentageLabel.text = "Average Percentage : N/A"
            SecondAvgPercentageLabel.font = UIFont (name: "Helvetica Neue", size: 10)
            SecondAvgPercentageLabel.textColor = UIColor.whiteColor()
            SecondView.addSubview(SecondAvgPercentageLabel)
            
            ///////////////////////////////////////////For First view three images views are below///////////////
            
            let xForDownloadFirstImageView = ((FirstView.frame.size.width/2)/5)
            let widthOfDownloadFirstImageView = (FirstimageView.frame.size.width/2.2)
            var xForCenterImageFirstView:CGFloat = CGFloat()
            var xForStarImageFirstView:CGFloat = CGFloat()
            var yForDownloadFirstImageView:CGFloat = CGFloat()
            
            if(screenSize.height == 568.0)
            {
                xForCenterImageFirstView = xForDownloadFirstImageView*3.8
                xForStarImageFirstView = xForDownloadFirstImageView*6.6
                yForDownloadFirstImageView = FirstAvgPercentageLabel.frame.origin.y*1.25
            }
            else if(screenSize.height == 736.0)
            {
               xForCenterImageFirstView = xForDownloadFirstImageView*3.7
               xForStarImageFirstView = xForDownloadFirstImageView*6.4
               yForDownloadFirstImageView = FirstAvgPercentageLabel.frame.origin.y*1.333
            }
            else if(screenSize.height == 667.0)
            {
              xForCenterImageFirstView = xForDownloadFirstImageView*3.7
              xForStarImageFirstView = xForDownloadFirstImageView*6.4
              yForDownloadFirstImageView = FirstAvgPercentageLabel.frame.origin.y*1.27
            }
            else if(screenSize.height == 480.0)
            {
                xForCenterImageFirstView = xForDownloadFirstImageView*3.7
                xForStarImageFirstView = xForDownloadFirstImageView*6.4
            }
            else
            {
                print("not an iphone device")
            }
            
            let DownloadFirstimageView:UIView = UIView()
//            let imageNameDownload = dataObj.objectAtIndex(i).valueForKey("DownloadImage") as? String
//            let imageDownload = UIImage(named:imageNameDownload!)
//            let DownloadFirstimageView = UIImageView(image: imageDownload!)
            DownloadFirstimageView.frame = CGRectMake(xForDownloadFirstImageView ,yForDownloadFirstImageView,widthOfDownloadFirstImageView,widthOfDownloadFirstImageView)
            FirstView.addSubview(DownloadFirstimageView)
            
//            ////////imageview in download view///////////////
////            let DownloadFirstimgView:UIImageView = UIImageView()
//                        let imgNameDownload = dataObj.objectAtIndex(i).valueForKey("DownloadImage") as? String
//                        let imgDownload = UIImage(named:imgNameDownload!)
//                        let DownloadFirstimgView = UIImageView(image: imgDownload!)
//            DownloadFirstimgView.frame = CGRectMake(10 ,10,40,40)
//            DownloadFirstimageView.addSubview(DownloadFirstimgView)
//            /////////////////////////////////////////

            
            let CenterFirstimageView:UIView = UIView()
//            let imageNameCenter = dataObj.objectAtIndex(i).valueForKey("centerImage") as? String
//            let imageCenter = UIImage(named:imageNameCenter!)
//            let CenterFirstimageView = UIImageView(image: imageCenter!)
            CenterFirstimageView.frame = CGRectMake(xForCenterImageFirstView ,yForDownloadFirstImageView,widthOfDownloadFirstImageView,widthOfDownloadFirstImageView)
            FirstView.addSubview(CenterFirstimageView)
            
            let StarFirstimageView:UIView = UIView()
//            let imageName = dataObj.objectAtIndex(i).valueForKey("starImage") as? String
//            let image = UIImage(named:imageName!)
//            let StarFirstimageView = UIImageView(image: image!)
            StarFirstimageView.frame = CGRectMake(xForStarImageFirstView ,yForDownloadFirstImageView,widthOfDownloadFirstImageView,widthOfDownloadFirstImageView)
            StarFirstimageView.backgroundColor = UIColor.cyanColor()
            FirstView.addSubview(StarFirstimageView)
  
            DownloadFirstimageView.layer.cornerRadius = DownloadFirstimageView.frame.size.height/2
            DownloadFirstimageView.layer.borderWidth = 2
            DownloadFirstimageView.layer.borderColor = UIColor.clearColor().CGColor
            DownloadFirstimageView.backgroundColor = UIColor(red:26.0/255.0, green:94.0/255.0, blue:139.0/255.0, alpha:1.0)
            DownloadFirstimageView.clipsToBounds = true
            
            CenterFirstimageView.layer.cornerRadius = CenterFirstimageView.frame.size.height/2
            CenterFirstimageView.layer.borderWidth = 2
            CenterFirstimageView.layer.borderColor = UIColor.clearColor().CGColor
            CenterFirstimageView.backgroundColor = UIColor(red:26.0/255.0, green:94.0/255.0, blue:139.0/255.0, alpha:1.0)
            CenterFirstimageView.clipsToBounds = true
            
            StarFirstimageView.layer.cornerRadius = StarFirstimageView.frame.size.height/2
            StarFirstimageView.layer.borderWidth = 2
            StarFirstimageView.layer.borderColor = UIColor.clearColor().CGColor
            StarFirstimageView.backgroundColor = UIColor(red:26.0/255.0, green:94.0/255.0, blue:139.0/255.0, alpha:1.0)
            StarFirstimageView.clipsToBounds = true
            
             ///////////////////////////////////////////For First view three images are below ///////////////
            
            let xOfDownloadFirstimgView = (DownloadFirstimageView.frame.size.width/2)/2
            let yOfDownloadFirstimgView = (DownloadFirstimageView.frame.size.height/2)/2
            let widthOfDownloadFirstimgView = (DownloadFirstimageView.frame.size.width/2)
            let heightOfDownloadFirstimgView = (DownloadFirstimageView.frame.size.height/2)
            
//            let DownloadFirstimgView:UIImageView = UIImageView()
            let imageNameFirstDownload = dataObj.objectAtIndex(i).valueForKey("DownloadImage") as? String
            let imageFirstDownload = UIImage(named:imageNameFirstDownload!)
            let DownloadFirstimgView = UIImageView(image: imageFirstDownload!)
            DownloadFirstimgView.frame = CGRectMake(xOfDownloadFirstimgView ,yOfDownloadFirstimgView,widthOfDownloadFirstimgView,heightOfDownloadFirstimgView)
//            DownloadFirstimgView.backgroundColor = UIColor.greenColor()
            DownloadFirstimageView.addSubview(DownloadFirstimgView)
            
            let xOfCenterFirstimgView = (CenterFirstimageView.frame.size.width/2)/2
            let yOfCenterFirstimgView = (CenterFirstimageView.frame.size.height/2)/2
            let widthOfCenterFirstimgView = (CenterFirstimageView.frame.size.width/2)
            let heightOfCenterFirstimgView = (CenterFirstimageView.frame.size.height/2)
            
//            let CenterFirstimgView:UIImageView = UIImageView()
            let imageNameimageCenterDownload = dataObj.objectAtIndex(i).valueForKey("centerImage") as? String
            let imageCenterFirstDownload = UIImage(named:imageNameimageCenterDownload!)
            let CenterFirstimgView = UIImageView(image: imageCenterFirstDownload!)
            
            CenterFirstimgView.frame = CGRectMake(xOfCenterFirstimgView ,yOfCenterFirstimgView,widthOfCenterFirstimgView,heightOfCenterFirstimgView)
//            CenterFirstimgView.backgroundColor = UIColor.greenColor()
            CenterFirstimageView.addSubview(CenterFirstimgView)
            
            let xOfStarFirstimgView = (StarFirstimageView.frame.size.width/2)/2
            let yOfStarFirstimgView = (StarFirstimageView.frame.size.height/2)/2
            let widthOfStarFirstimgView = (StarFirstimageView.frame.size.width/2)
            let heightOfStarFirstimgView = (StarFirstimageView.frame.size.height/2)
            
//            let StarFirstimgView:UIImageView = UIImageView()
            let imageNamestarFirstDownload = dataObj.objectAtIndex(i).valueForKey("starImage") as? String
            let imageStarFirstDownload = UIImage(named:imageNamestarFirstDownload!)
            let StarFirstimgView = UIImageView(image: imageStarFirstDownload!)
            
            StarFirstimgView.frame = CGRectMake(xOfStarFirstimgView ,yOfStarFirstimgView,widthOfStarFirstimgView,heightOfStarFirstimgView)
//            StarFirstimgView.backgroundColor = UIColor.greenColor()
            StarFirstimageView.addSubview(StarFirstimgView)
            
            ///////////////////////////For Second view three images are below//////////////////
            
            let xForDownloadSecondImageView = ((SecondView.frame.size.width/2)/4.9)
            var yForDownloadSecondImageView:CGFloat = CGFloat()
            let widthOfDownloadSecondImageView = (SecondimageView.frame.size.width/2.1)
            
            var xForCenterImageSecondView:CGFloat = CGFloat()
            var xForStarImageSecondView:CGFloat = CGFloat()
            
            if(screenSize.height == 568.0)
            {
                xForCenterImageSecondView = xForDownloadSecondImageView*3.8
                xForStarImageSecondView = xForDownloadSecondImageView*6.6
                yForDownloadSecondImageView = SecondAvgPercentageLabel.frame.origin.y*1.28
            }
            else if(screenSize.height == 736.0)
            {
                xForCenterImageSecondView = xForDownloadSecondImageView*3.7
                xForStarImageSecondView = xForDownloadSecondImageView*6.4
                yForDownloadSecondImageView = SecondAvgPercentageLabel.frame.origin.y*1.36
            }
            else if(screenSize.height == 667.0)
            {
                xForCenterImageSecondView = xForDownloadSecondImageView*3.7
                xForStarImageSecondView = xForDownloadSecondImageView*6.4
                yForDownloadSecondImageView = SecondAvgPercentageLabel.frame.origin.y*1.3
            }
            else if(screenSize.height == 480.0)
            {
                xForCenterImageSecondView = xForDownloadSecondImageView*4.2
                xForStarImageSecondView = xForDownloadSecondImageView*6.8
            }
            else
            {
                print("not an iphone device")
            }

            print(xForCenterImageSecondView)
            
            let DownloadSecondimageView:UIView = UIView()
//            let imageNamesecondDownload = dataObj.objectAtIndex(i).valueForKey("DownloadImage") as? String
//            let imagesecondDownload = UIImage(named:imageNamesecondDownload!)
//            let DownloadSecondimageView = UIImageView(image: imagesecondDownload!)
            DownloadSecondimageView.frame = CGRectMake(xForDownloadSecondImageView ,yForDownloadSecondImageView,widthOfDownloadSecondImageView,widthOfDownloadSecondImageView)
            DownloadSecondimageView.backgroundColor = UIColor.cyanColor()
            SecondView.addSubview(DownloadSecondimageView)
            
            let CenterSecondimageView:UIView = UIView()
//            let imageNamesecondCenter = dataObj.objectAtIndex(i).valueForKey("centerImage") as? String
//            let imagesecondCenter = UIImage(named:imageNamesecondCenter!)
//            let CenterSecondimageView = UIImageView(image: imagesecondCenter!)
            CenterSecondimageView.frame = CGRectMake(xForCenterImageSecondView ,yForDownloadSecondImageView,widthOfDownloadSecondImageView,widthOfDownloadSecondImageView)
            CenterSecondimageView.backgroundColor = UIColor.cyanColor()
            SecondView.addSubview(CenterSecondimageView)
            
            let StarSecondImageView:UIView = UIView()
//            let imageNamesecondStar = dataObj.objectAtIndex(i).valueForKey("starImage") as? String
//            let imagesecondStar = UIImage(named:imageNamesecondStar!)
//            let StarSecondImageView = UIImageView(image: imagesecondStar!)
            StarSecondImageView.frame = CGRectMake(xForStarImageSecondView ,yForDownloadSecondImageView,widthOfDownloadSecondImageView,widthOfDownloadSecondImageView)
            StarSecondImageView.backgroundColor = UIColor(red:26.0/255.0, green:94.0/255.0, blue:139.0/255.0, alpha:1.0)

            SecondView.addSubview(StarSecondImageView)
            
            DownloadSecondimageView.layer.cornerRadius = DownloadSecondimageView.frame.size.height/2
            DownloadSecondimageView.layer.borderWidth = 2
            DownloadSecondimageView.layer.borderColor = UIColor.clearColor().CGColor
            DownloadSecondimageView.backgroundColor = UIColor(red:26.0/255.0, green:94.0/255.0, blue:139.0/255.0, alpha:1.0)
            DownloadSecondimageView.clipsToBounds = true
            
            CenterSecondimageView.layer.cornerRadius = CenterSecondimageView.frame.size.height/2
            CenterSecondimageView.layer.borderWidth = 2
            CenterSecondimageView.layer.borderColor = UIColor.clearColor().CGColor
            CenterSecondimageView.backgroundColor = UIColor(red:26.0/255.0, green:94.0/255.0, blue:139.0/255.0, alpha:1.0)
            CenterSecondimageView.clipsToBounds = true
            
            StarSecondImageView.layer.cornerRadius = StarSecondImageView.frame.size.height/2
            StarSecondImageView.layer.borderWidth = 4
            StarSecondImageView.layer.borderColor = UIColor.clearColor().CGColor
            CenterSecondimageView.backgroundColor = UIColor(red:26.0/255.0, green:94.0/255.0, blue:139.0/255.0, alpha:1.0)
            CenterSecondimageView.clipsToBounds = true
            
            ///////////////////////////////////////////For Second view three images are below ///////////////
            
            let xOfDownloadSecondimgView = (DownloadSecondimageView.frame.size.width/2)/2
            let yOfDownloadSecondimgView = (DownloadSecondimageView.frame.size.height/2)/2
            let widthOfDownloadSecondimgView = (DownloadSecondimageView.frame.size.width/2)
            let heightOfDownloadSecondimgView = (DownloadSecondimageView.frame.size.height/2)
            
//            let DownloadSecondimgView:UIImageView = UIImageView()
            let imageNameDownloadSecondDownload = dataObj.objectAtIndex(i).valueForKey("DownloadImage") as? String
            let imageDownloadSecondDownload = UIImage(named:imageNameDownloadSecondDownload!)
            let DownloadSecondimgView:UIImageView = UIImageView(image: imageDownloadSecondDownload!)
            DownloadSecondimgView.frame = CGRectMake(xOfDownloadSecondimgView ,yOfDownloadSecondimgView,widthOfDownloadSecondimgView,heightOfDownloadSecondimgView)
//            DownloadSecondimgView.backgroundColor = UIColor.greenColor()
            DownloadSecondimageView.addSubview(DownloadSecondimgView)
            
            let xOfCenterSecondimgView = (CenterSecondimageView.frame.size.width/2)/2
            let yOfCenterSecondimgView = (CenterSecondimageView.frame.size.height/2)/2
            let widthOfCenterSecondimgView = (CenterSecondimageView.frame.size.width/2)
            let heightOfCenterSecondimgView = (CenterSecondimageView.frame.size.height/2)
            
//            let CenterSecondimgView:UIImageView = UIImageView()
            let imageNameCenterSecondDownload = dataObj.objectAtIndex(i).valueForKey("centerImage") as? String
            let imageDownloadCenterDownload = UIImage(named:imageNameCenterSecondDownload!)
            let CenterSecondimgView:UIImageView = UIImageView(image: imageDownloadCenterDownload!)
            
            CenterSecondimgView.frame = CGRectMake(xOfCenterSecondimgView ,yOfCenterSecondimgView,widthOfCenterSecondimgView,heightOfCenterSecondimgView)
//            CenterSecondimgView.backgroundColor = UIColor.greenColor()
            CenterSecondimageView.addSubview(CenterSecondimgView)
            
            let xOfStarSecondImagView = (StarSecondImageView.frame.size.width/2)/2
            let yOfStarSecondImagView = (StarSecondImageView.frame.size.height/2)/2
            let widthOfStarSecondImagView = (StarSecondImageView.frame.size.width/2)
            let heightOfStarSecondImagView = (StarSecondImageView.frame.size.height/2)
            
//            let StarSecondImagView:UIImageView = UIImageView()
            let imageNameStarSecond = dataObj.objectAtIndex(i).valueForKey("starImage") as? String
            let imageStarSecond = UIImage(named:imageNameStarSecond!)
            let StarSecondImagView:UIImageView = UIImageView(image: imageStarSecond!)
            
            StarSecondImagView.frame = CGRectMake(xOfStarSecondImagView ,yOfStarSecondImagView,widthOfStarSecondImagView,heightOfStarSecondImagView)
//            StarSecondImagView.backgroundColor = UIColor.greenColor()
            StarSecondImageView.addSubview(StarSecondImagView)
            
            
            ////////////////////////////////////////////////////////////////////////////////////
            
            let widthOfLowerFirstLabel = FirstView.frame.size.width
            var yOfLowerFirstLabel:CGFloat = CGFloat()
            
            if(screenSize.height == 736.0)
            {
                yOfLowerFirstLabel = DownloadFirstimageView.frame.origin.y + 66
            }
            else if(screenSize.height == 667.0)
            {
                yOfLowerFirstLabel = DownloadFirstimageView.frame.origin.y + 52
            }
            else if(screenSize.height == 568.0)
            {
                yOfLowerFirstLabel = DownloadFirstimageView.frame.origin.y + 40
            }
            else if(screenSize.height == 480.0)
            {
                yOfLowerFirstLabel = DownloadFirstimageView.frame.origin.y + 25
            }
 
            var LowerFirstLabel = UILabel(frame: CGRectMake(0,yOfLowerFirstLabel,widthOfLowerFirstLabel,20))
            LowerFirstLabel.textAlignment = NSTextAlignment.Center
            LowerFirstLabel.text = dataObj.objectAtIndex(i).valueForKey("score") as? String
            LowerFirstLabel.font = UIFont (name: "HelveticaNeue-Bold", size: 11)
            LowerFirstLabel.textColor = UIColor.whiteColor()
            FirstView.addSubview(LowerFirstLabel)
            
            //////////////////////////////////
            
            let widthOfLowerSecondLabel = FirstView.frame.size.width
            var yOfLowerSecondLabel:CGFloat = CGFloat()
            
            if(screenSize.height == 736.0)
            {
                yOfLowerSecondLabel = DownloadSecondimageView.frame.origin.y + 66
            }
            else if(screenSize.height == 667.0)
            {
                yOfLowerSecondLabel = DownloadSecondimageView.frame.origin.y + 52
            }
            else if(screenSize.height == 568.0)
            {
              yOfLowerSecondLabel = DownloadSecondimageView.frame.origin.y + 40
            }
            
            var LowerSecondLabel = UILabel(frame: CGRectMake(0,yOfLowerSecondLabel,widthOfLowerSecondLabel,20))
            LowerSecondLabel.textAlignment = NSTextAlignment.Center
            LowerSecondLabel.text = dataObj.objectAtIndex(i).valueForKey("score") as? String
            LowerSecondLabel.font = UIFont (name: "HelveticaNeue-Bold", size: 11)
            LowerSecondLabel.textColor = UIColor.whiteColor()
            SecondView.addSubview(LowerSecondLabel)
            
            
            //////////Corner ImageView First View ////////

            let xOfCornerFirstImageView = FirstView.frame.size.width*0.8
            let widthOfCornerFirstImageView = (FirstimageView.frame.size.width/3)
            
//            let CornerFirstImageView:UIImageView = UIImageView()
            
            let CornerFirstImagViewName = dataObj.objectAtIndex(i).valueForKey("plusImage") as? String
            let CornerFirstImag = UIImage(named:CornerFirstImagViewName!)
            let CornerFirstImageView:UIImageView = UIImageView(image: CornerFirstImag!)
            
            CornerFirstImageView.frame = CGRectMake(xOfCornerFirstImageView ,5,widthOfCornerFirstImageView,widthOfCornerFirstImageView)
            CornerFirstImageView.backgroundColor = UIColor.cyanColor()
            FirstView.addSubview(CornerFirstImageView)
            if(i == 0)
            {
                CornerFirstImageView.hidden = false
            }
            else
            {
                CornerFirstImageView.hidden = true
            }
            
            CornerFirstImageView.layer.cornerRadius = CornerFirstImageView.frame.size.height/2
            CornerFirstImageView.layer.borderWidth = 4
            CornerFirstImageView.layer.borderColor = UIColor.clearColor().CGColor
            CornerFirstImageView.backgroundColor = UIColor.redColor()
            CornerFirstImageView.clipsToBounds = true
            
//                 //////////Corner ImageView First View ////////
//            
//            let xOfCornerFirstImagView = (CornerFirstImageView.frame.size.width/2)/2
//            let yOfCornerFirstImagView = (CornerFirstImageView.frame.size.height/2)/2
//            let widthOfCornerFirstImagView = (CornerFirstImageView.frame.size.width/2)
//            let heightOfCornerFirstImagView = (CornerFirstImageView.frame.size.height/2)
//            
//            //            let StarSecondImagView:UIImageView = UIImageView()
//            let CornerFirstImagViewName = dataObj.objectAtIndex(i).valueForKey("plusImage") as? String
//            let CornerFirstImag = UIImage(named:CornerFirstImagViewName!)
//            let CornerFirstImagView:UIImageView = UIImageView(image: CornerFirstImag!)
//            
//            CornerFirstImagView.frame = CGRectMake(xOfCornerFirstImagView ,yOfCornerFirstImagView,widthOfCornerFirstImagView,heightOfCornerFirstImagView)
//            //            StarSecondImagView.backgroundColor = UIColor.greenColor()
//            CornerFirstImageView.addSubview(CornerFirstImagView)
//     
//            //////////////////////////////////////////////////////////
            
        }
        
        func scrollViewDidScroll(scrollView: UIScrollView)
        {
            let pageHeight = CGFloat(statsScrollView.frame.size.height); // you need to have a iVar with getter for scrollView
            let fractionalPage = scrollView.contentOffset.y / pageHeight;
            
            let page = Double(fractionalPage);
            index = Int(page);
            
            
            print("index is \(index)");
        }

        
        
    }
}
