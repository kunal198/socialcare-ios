
//
//  SCRequestingCourse.swift
//  SocialCare
//
//  Created by Mrinal Khullar on 4/27/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class SCRequestingCourse: UIViewController
{

    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0
    
    @IBOutlet weak var requestView: UIView!
    @IBOutlet weak var requestCourseOutlet: UIButton!
    @IBOutlet weak var messageTxtField: UITextField!
    @IBOutlet weak var adpotingTxtField: UITextField!
    @IBOutlet weak var childrenHomeTxtField: UITextField!
    @IBOutlet weak var onlineTxtField: UITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        placeHolderMethodForRequesting()
        
        requestView.layer.cornerRadius = 8
        requestView.layer.borderWidth = 4
        requestView.layer.borderColor = UIColor.clearColor().CGColor
        
        requestCourseOutlet.layer.cornerRadius = 8
        requestCourseOutlet.layer.borderWidth = 4
        requestCourseOutlet.layer.borderColor = UIColor.clearColor().CGColor

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool)
    {
         closeMenuBtn.hidden = true
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
    }
    
    func placeHolderMethodForRequesting()
    {
        let paddingView = UIView(frame: CGRectMake(0, -15, 15, messageTxtField.frame.height))
        messageTxtField.leftView = paddingView
        messageTxtField.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewPwd = UIView(frame: CGRectMake(0, 0, 15, adpotingTxtField.frame.height))
        adpotingTxtField.leftView = paddingViewPwd
        adpotingTxtField.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewChildren = UIView(frame: CGRectMake(0, 0, 15, childrenHomeTxtField.frame.height))
        childrenHomeTxtField.leftView = paddingViewChildren
        childrenHomeTxtField.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewOnline = UIView(frame: CGRectMake(0, 0, 15, onlineTxtField.frame.height))
        onlineTxtField.leftView = paddingViewOnline
        onlineTxtField.leftViewMode = UITextFieldViewMode.Always
        
        
        messageTxtField.attributedPlaceholder = NSAttributedString(string:"ENTER MESSAGE", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
        adpotingTxtField.attributedPlaceholder = NSAttributedString(string:"ADOPTION FOSTERING", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
        
        childrenHomeTxtField.attributedPlaceholder = NSAttributedString(string:"CHILDREN HOMES ONLINE", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
        onlineTxtField.attributedPlaceholder = NSAttributedString(string:"ONLINE", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
    }
    
    @IBAction func menuBtn(sender: AnyObject)
    {
        myMenuView.hidden = false
        
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
        else
        {
            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.5, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })
    }
    @IBAction func backBtn(sender: AnyObject) {
    }

    @IBAction func requestBtn(sender: AnyObject) {
    }
}
