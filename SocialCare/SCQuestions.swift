//
//  SCQuestions.swift
//  SocialCare
//
//  Created by Mrinal Khullar on 4/26/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class SCQuestions: UIViewController
{
    
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0

    @IBOutlet weak var optionDImageView: UIImageView!
    @IBOutlet weak var optionCImageVIew: UIImageView!
    @IBOutlet weak var optionBImageView: UIImageView!
    
    @IBOutlet weak var optionAImageView: UIImageView!
    @IBOutlet weak var redQuestionView: UIView!
    @IBOutlet weak var saveOutlet: UIButton!
    @IBOutlet weak var saveBtnView: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        saveOutlet.layer.cornerRadius = 8
        saveOutlet.layer.borderWidth = 4
        saveOutlet.layer.borderColor = UIColor.clearColor().CGColor
        
        saveBtnView.layer.cornerRadius = 8
        saveBtnView.layer.borderWidth = 4
        saveBtnView.layer.borderColor = UIColor.clearColor().CGColor
        
        redQuestionView.layer.cornerRadius = 8
        redQuestionView.layer.borderWidth = 4
        redQuestionView.layer.borderColor = UIColor.clearColor().CGColor

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool)
    {
         closeMenuBtn.hidden = true
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
    }

    @IBAction func menuBtn(sender: AnyObject)
    {
        myMenuView.hidden = false
        
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
        else
        {
            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.5, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveBtn(sender: AnyObject)
    {
        isUnitStartedVisible = true
        
        let SCStartCoursez = self.storyboard!.instantiateViewControllerWithIdentifier("SCStartCourse") as! SCStartCourse
        
        self.navigationController?.pushViewController(SCStartCoursez, animated: true)
    }

    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func optionsBtn(sender: AnyObject)
    {
        if(sender.tag == 10)
        {
            optionAImageView.image = UIImage(named:"q1.png")
            optionBImageView.image = UIImage(named:"q2.png")
            optionCImageVIew.image = UIImage(named:"q2.png")
            optionDImageView.image = UIImage(named:"q2.png")
            
        }
        else if(sender.tag == 11)
        {
            optionAImageView.image = UIImage(named:"q2.png")
            optionBImageView.image = UIImage(named:"q1.png")
            optionCImageVIew.image = UIImage(named:"q2.png")
            optionDImageView.image = UIImage(named:"q2.png")
        }
        else if(sender.tag == 12)
        {
            optionAImageView.image = UIImage(named:"q2.png")
            optionBImageView.image = UIImage(named:"q2.png")
            optionCImageVIew.image = UIImage(named:"q1.png")
            optionDImageView.image = UIImage(named:"q2.png")
        }
        else
        {
            optionAImageView.image = UIImage(named:"q2.png")
            optionBImageView.image = UIImage(named:"q2.png")
            optionCImageVIew.image = UIImage(named:"q2.png")
            optionDImageView.image = UIImage(named:"q1.png")
        }
    }

}
