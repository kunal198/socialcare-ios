//
//  SCCustomCell.swift
//  SocialCare
//
//  Created by Mrinal Khullar on 4/26/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class SCCustomCell: UITableViewCell
{
    
    @IBOutlet weak var signOffCover: UIView!
    @IBOutlet weak var startUnitCoverView: UIView!
    @IBOutlet weak var whiteRedView: UIView!
    @IBOutlet weak var sentCoverView: UIView!
    @IBOutlet weak var sentImageView: UIImageView!
    @IBOutlet weak var sentDay: UILabel!
    @IBOutlet weak var sentDate: UILabel!
    @IBOutlet weak var sentFrom: UILabel!
    @IBOutlet weak var sentDesc: UILabel!
    @IBOutlet weak var sentTitle: UILabel!
    @IBOutlet weak var whiteSelectingView: UIView!
    //////////////////////////////////////
    @IBOutlet weak var personalImageView: UIImageView!
    @IBOutlet weak var deleteOutlet: UIButton!
    @IBOutlet weak var favorite: UILabel!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var timepersonal: UILabel!
    @IBOutlet weak var statusDescLabel: UILabel!
    @IBOutlet weak var personalBlueView: UIView!
    //////////////////////////////////////////////
    @IBOutlet weak var redoOultet: UIButton!
    @IBOutlet weak var answer: UILabel!

    @IBOutlet weak var correctInCorrectLbl: UIButton!
    @IBOutlet weak var quesName: UILabel!
    @IBOutlet weak var questionNo: UILabel!
    @IBOutlet weak var yellowUnitAbout: UILabel!
    @IBOutlet weak var unitNumber: UILabel!
    
    @IBOutlet weak var startOff: UIButton!
    
    @IBOutlet weak var unitAbout: UILabel!
    @IBOutlet weak var startUnitLbl: UIButton!
    @IBOutlet weak var unitView: UIView!
    ///////////////////////////////////////////////
    @IBOutlet weak var courseCellView: UIView!
    @IBOutlet weak var startCourseLbl: UIButton!
    @IBOutlet weak var coursecompletedLbl: UIButton!
    @IBOutlet weak var handbookLbl: UIButton!
    @IBOutlet weak var courseTitle: UILabel!
    
    @IBOutlet weak var courseDescription: UITextView!
    @IBOutlet weak var courseReviews: UILabel!
    @IBOutlet weak var courseImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
        // Configure the view for the selected state
    }

}
