//
//  SCDownloadHandbook.swift
//  SocialCare
//
//  Created by Mrinal Khullar on 4/27/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class SCDownloadHandbook: UIViewController
{

   
    @IBOutlet weak var lowerRedView: UIView!
    @IBOutlet weak var lowerCoverView: UIView!
    @IBOutlet weak var downloadOutlet: UIButton!
    @IBOutlet weak var yellowCoverView: UIView!
    @IBOutlet weak var redView: UIView!
    @IBOutlet weak var redCoverView: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        lowerRedView.layer.cornerRadius = 8
        lowerRedView.layer.borderWidth = 4
        lowerRedView.layer.borderColor = UIColor.clearColor().CGColor
        
        lowerCoverView.layer.cornerRadius = 8
        lowerCoverView.layer.borderWidth = 4
        lowerCoverView.layer.borderColor = UIColor.clearColor().CGColor

        
        downloadOutlet.layer.cornerRadius = 8
        downloadOutlet.layer.borderWidth = 4
        downloadOutlet.layer.borderColor = UIColor.clearColor().CGColor
        
        yellowCoverView.layer.cornerRadius = 8
        yellowCoverView.layer.borderWidth = 4
        yellowCoverView.layer.borderColor = UIColor.clearColor().CGColor
        
        redView.layer.cornerRadius = 8
        redView.layer.borderWidth = 4
        redView.layer.borderColor = UIColor.clearColor().CGColor
        
        redCoverView.layer.cornerRadius = 8
        redCoverView.layer.borderWidth = 4
        redCoverView.layer.borderColor = UIColor.clearColor().CGColor

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menuBtn(sender: AnyObject) {
    }
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func nextBtn(sender: AnyObject)
    {
        isUnitFour = true
        let SCStartCoursez = self.storyboard!.instantiateViewControllerWithIdentifier("SCStartCourse") as! SCStartCourse
        
        self.navigationController?.pushViewController(SCStartCoursez, animated: true)
    }
}
