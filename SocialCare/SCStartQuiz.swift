//
//  SCStartQuiz.swift
//  SocialCare
//
//  Created by Mrinal Khullar on 4/26/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class SCStartQuiz: UIViewController
{
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0
    
    @IBOutlet weak var quizBtnOutlet: UIButton!
    @IBOutlet weak var yellowCoverView: UIView!
    @IBOutlet weak var redView: UIView!
    @IBOutlet weak var coverRedView: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        quizBtnOutlet.layer.cornerRadius = 8
        quizBtnOutlet.layer.borderWidth = 4
        quizBtnOutlet.layer.borderColor = UIColor.clearColor().CGColor
        
        
        yellowCoverView.layer.cornerRadius = 8
        yellowCoverView.layer.borderWidth = 4
        yellowCoverView.layer.borderColor = UIColor.clearColor().CGColor
        
        redView.layer.cornerRadius = 8
        redView.layer.borderWidth = 4
        redView.layer.borderColor = UIColor.clearColor().CGColor
        
        coverRedView.layer.cornerRadius = 8
        coverRedView.layer.borderWidth = 4
        coverRedView.layer.borderColor = UIColor.clearColor().CGColor


        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool)
    {
         closeMenuBtn.hidden = true
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menuBtn(sender: AnyObject)
    {
        myMenuView.hidden = false
        
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
        else
        {
            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.5, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })
    }
    @IBAction func quizBtn(sender: AnyObject)
    {
        
        let SCQuestionsz = self.storyboard!.instantiateViewControllerWithIdentifier("SCQuestions") as! SCQuestions
        self.navigationController?.pushViewController(SCQuestionsz, animated: true)
    }

    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
